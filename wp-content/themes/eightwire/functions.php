<?php

// Safety first.
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'functions.php' == basename($_SERVER['SCRIPT_FILENAME'])) { die(); }

/************* PREVENTS PINGBACK DDOS *************/
add_filter('xmlrpc_methods', function($methods) {
   unset($methods['pingback.ping']);
   return $methods;
});


/************* SETUP THE THEME *************/
function ew_setup() {
	//Add title support
	add_theme_support('title-tag');

	//Register menus
	register_nav_menus(array(
		'primary-menu' => 'Primary',
		'secondary-menu' => 'Secondary',
        'shop-menu' => 'Shop Menu'
	));

	//Enable editor role to edit menu
	$role_object = get_role('editor');
	$role_object->add_cap('edit_theme_options');
	$role_object->add_cap('delete_media');
	$role_object = get_role('shop_manager');
	$role_object->add_cap('edit_theme_options');
	$role_object->add_cap('delete_media');

	//Featured image support
	add_theme_support('post-thumbnails');
	
	//Prevents the built-in Small, Medium, Large image sizes
	foreach ( get_intermediate_image_sizes() as $size ) {
		remove_image_size( $size );
	}
	//Prevents the medium_large image size
	update_option('medium_large_size_w',0);
	update_option('medium_large_size_h',0);

	//Generates specific image sizes from the uploaded original
	add_image_size('image', 1920, 1080, true);
	add_image_size('banner', 1920, 600, true);
	add_image_size('single', 320, 240, true);
    add_image_size('media', 640, 480, true);
    add_image_size('ti-image', 640, 350, true);
    add_image_size('gi-image', 325, 500, true);
    add_image_size('insp-banner', 1920, 400, true);

	//Register sidebars
	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name'			=> 'sidebar1',
			'id'            => 'sidebar1',
			'before_widget'	=> '<section class="widget">',
			'after_widget' 	=> '</section>',
			'before_title'	=> '<h3 class="widgettitle">',
			'after_title'	=> '</h3>'
		));
	}

	//Support WooCom
	add_theme_support('woocommerce');
	add_theme_support('wc-product-gallery-zoom');
	add_theme_support('wc-product-gallery-lightbox');
	add_theme_support('wc-product-gallery-slider');

	//Register ACF options page
	if (function_exists('acf_add_options_page')) {
		acf_add_options_page(array(
			'page_title' 	=> 'Theme options',
			'menu_title'	=> 'Theme options',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

	/************* REMOVE HEAD JUNK *************/
	add_filter( 'the_generator', create_function('$a', "return null;") );
	remove_action('wp_head', 'wp_generator'); //removes WP Version # for security
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	//remove_action('wp_head', 'feed_links', 2);
	//remove_action('wp_head', 'feed_links_extra', 3);
	//DEPRECATED
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
}
add_action('after_setup_theme', 'ew_setup');


/************* ENQUEUE STYLES AND SCRIPTS *************/
if (!is_admin()) {
	function ew_enqueue_style() {
		wp_register_style('reset', get_stylesheet_directory_uri().'/assets/css/reset.css', false);
		wp_enqueue_style('reset');

		wp_register_style('eightwire', get_stylesheet_uri(), 'reset');
		wp_enqueue_style('eightwire');

		wp_enqueue_style('flexslider-css', (get_template_directory_uri() . '/assets/css/flexslider.css'), 'flexslider', '2.6.3');

		wp_register_style('magnific-css', get_stylesheet_directory_uri().'/assets/js/magnific/magnific-popup.css', 'magnific', '1.1.0');
		wp_enqueue_style('magnific-css');

		//wp_register_style('fontastic', get_stylesheet_directory_uri().'/assets/css/fontastic.css', false);
		//wp_enqueue_style('fontastic');
	}
	add_action('wp_enqueue_scripts', 'ew_enqueue_style');

	function ew_enqueue_script() {
		wp_register_script('html5shiv', (get_template_directory_uri() . '/assets/js/html5shiv.min.js'), '', '3.7.2', true);
		wp_enqueue_script('html5shiv');

		wp_register_script('bowser', (get_template_directory_uri() . '/assets/js/bowser.min.js'), '', '0.7.2', true);
		wp_enqueue_script('bowser');

		wp_register_script('runbowser', (get_template_directory_uri() . '/assets/js/runbowser.js'), 'bowser', '1.0.0', true);
		wp_enqueue_script('runbowser');

		wp_register_script('flexslider', (get_template_directory_uri() . '/assets/js/jquery.flexslider-min.js'), array('jquery'), '2.6.3', true);
		wp_enqueue_script('flexslider');

		wp_register_script('magnific', (get_template_directory_uri() . '/assets/js/magnific/jquery.magnific-popup.min.js'), array('jquery'), '1.1.0', true);
		wp_enqueue_script('magnific');

		wp_register_script('matchheight', (get_template_directory_uri() . '/assets/js/jquery.matchHeight-min.js'), array('jquery'), '0.7.2', true);
		wp_enqueue_script('matchheight');

		wp_register_script('ewscripts', (get_template_directory_uri() . '/assets/js/ewscripts.js'), array('jquery', 'magnific', 'matchheight', 'flexslider'), '4.0.0', true);
		wp_enqueue_script('ewscripts');

		wp_register_script('ewmodal', (get_template_directory_uri() . '/assets/js/modal.js'), array('jquery'), '1.0.0', true);
		wp_enqueue_script('ewmodal');
	}
add_action('wp_enqueue_scripts', 'ew_enqueue_script');
}


/************* GOOGLE FONTS *************/
function ew_googlefonts() {
    wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600');
    wp_enqueue_style('googleFonts');
}
add_action('wp_print_styles', 'ew_googlefonts');

/************* CHANGE WP ADMIN BAR HOWDY TO HI *************/
function replace_howdy( $wp_admin_bar ) {
	$my_account=$wp_admin_bar->get_node('my-account');
	$newtitle = str_replace( 'Howdy,', '', $my_account->title );
	$newtitle = str_replace( 'How are you,', '', $newtitle );
	$newtitle = str_replace( 'Hi,', '', $newtitle );
	$newtitle = str_replace( '?', '', $newtitle );
	$wp_admin_bar->add_node( array(
	'id' => 'my-account',
	'title' => $newtitle,
	) );
}
add_filter( 'admin_bar_menu', 'replace_howdy',25 );

/************* REMOVE STUFF FROM ADMIN BAR *************/
function ew_remove_wpadminbar_items( $wp_admin_bar ) {
	$wp_admin_bar->remove_menu( 'customize' );
	$wp_admin_bar->remove_menu( 'comments' );
	//$wp_admin_bar->remove_menu( 'new-post' );
	$wp_admin_bar->remove_node( 'wp-logo' );
	$wp_admin_bar->remove_node( 'mail_bank' );
	$wp_admin_bar->remove_node( 'wpseo-menu' );
	$wp_admin_bar->remove_node( 'new-content' );
	$wp_admin_bar->remove_node( 'dashboard' );
	$wp_admin_bar->remove_node( 'menus' );
	$wp_admin_bar->remove_node( 'themes' );
	$wp_admin_bar->remove_node( 'widgets' );
	$wp_admin_bar->remove_node( 'search' );
	$wp_admin_bar->remove_node( 'updates' );
}
add_action( 'admin_bar_menu', 'ew_remove_wpadminbar_items', 999 );

/*****SVG FUNCTIONS*******/
function facebook_svg() {
    $facebook = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.49 35.49"><defs><style>.cls-1{fill:#d2a339;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M17.65,0h.2A17.62,17.62,0,0,1,27.93,3.18a19.31,19.31,0,0,1,2,1.66A17.6,17.6,0,0,1,31.59,6.6a18.43,18.43,0,0,1,1.48,2.15,17.5,17.5,0,0,1,2.42,8.9v.2a17.68,17.68,0,0,1-3.17,10.08,20.87,20.87,0,0,1-1.67,2,17.6,17.6,0,0,1-1.76,1.64,19.34,19.34,0,0,1-2.14,1.48,17.55,17.55,0,0,1-8.9,2.42h-.2A17.71,17.71,0,0,1,7.56,32.32a19.68,19.68,0,0,1-2-1.67,16.43,16.43,0,0,1-1.64-1.76,19.64,19.64,0,0,1-1.49-2.14A17.65,17.65,0,0,1,0,17.85v-.2A17.65,17.65,0,0,1,3.18,7.56a18.28,18.28,0,0,1,1.66-2A16.43,16.43,0,0,1,6.6,3.91,18.7,18.7,0,0,1,8.75,2.42,17.59,17.59,0,0,1,17.65,0Zm1.86,5.87-1.06.41a4.75,4.75,0,0,0-2.42,2,6.21,6.21,0,0,0-.8,3.2v3.3H11.47v4.34h3.76V30.3h4.48V19.16h3.76c0-.12.2-1.56.55-4.34H19.71V12.38a4.92,4.92,0,0,1,.1-1.26,1.26,1.26,0,0,1,.18-.43,1.38,1.38,0,0,1,.81-.61,4.25,4.25,0,0,1,1.38-.15h2V6.05a28.31,28.31,0,0,0-3.25-.18H19.51Z"/></g></g></svg>';
    
    return $facebook;
}

function twitter_svg() {
    $twitter = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.49 35.49"><defs><style>.cls-1{fill:#d2a339;}</style></defs><title>Asset 6</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M17.72,0h.05A17.63,17.63,0,0,1,34.36,11.52a17.32,17.32,0,0,1,1.13,6.2v.05A17.63,17.63,0,0,1,24,34.36a17.28,17.28,0,0,1-6.2,1.13h-.05A17.66,17.66,0,0,1,1.13,24,17.5,17.5,0,0,1,0,17.77v-.05A17.66,17.66,0,0,1,11.52,1.13,17.54,17.54,0,0,1,17.72,0Zm.91,13.92V14a4.81,4.81,0,0,0,.13,1h0a15.59,15.59,0,0,1-2.07-.25,16,16,0,0,1-2.32-.71A14.07,14.07,0,0,1,12,12.88a14.15,14.15,0,0,1-2.09-1.66c-.4-.41-.77-.8-1.11-1.19a4.94,4.94,0,0,0-.63,2.32v.2a4.73,4.73,0,0,0,1.14,3,3.59,3.59,0,0,0,1,.86v0a5,5,0,0,1-2.15-.58V16A4.71,4.71,0,0,0,9.6,19.36a4.56,4.56,0,0,0,1.69,1.06,3.09,3.09,0,0,0,.66.2s-.18.08-.53.13a4.33,4.33,0,0,1-.5.05h-.28a5,5,0,0,1-.81-.08,4.78,4.78,0,0,0,2,2.55,4.61,4.61,0,0,0,2.49.78,7.17,7.17,0,0,1-1.36.91,9.7,9.7,0,0,1-4.41,1.13H8.19c-.32,0-.64,0-1-.05a12.32,12.32,0,0,0,2.92,1.41,13.5,13.5,0,0,0,4.47.76,13.86,13.86,0,0,0,6.75-1.69A13.5,13.5,0,0,0,23.49,25,12.91,12.91,0,0,0,25,23.44,15,15,0,0,0,26.62,21a14.35,14.35,0,0,0,.86-1.94,13.68,13.68,0,0,0,.78-4.36v-.83a9.68,9.68,0,0,0,1.33-1.16,9.37,9.37,0,0,0,1.06-1.31h0a12.22,12.22,0,0,1-1.89.6c-.49.09-.76.13-.81.13v0a4.72,4.72,0,0,0,1-.79A5,5,0,0,0,30,9.5a9.44,9.44,0,0,1-2,.89,6.28,6.28,0,0,1-1.08.27,4.32,4.32,0,0,0-1-.8,4.61,4.61,0,0,0-2.44-.71h0a4.73,4.73,0,0,0-3.37,1.39A4.67,4.67,0,0,0,18.63,13.92Z"/></g></g></svg>';
    
    return $twitter;
}

function instagram_svg() {
    $insta = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.49 35.49"><defs><style>.cls-1{fill:#d2a339;}</style></defs><title>Asset 7</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M17.7,0h.1A17.61,17.61,0,0,1,28,3.28a20,20,0,0,1,1.72,1.38A18.18,18.18,0,0,1,31.31,6.3a15.8,15.8,0,0,1,1.94,2.83,17.27,17.27,0,0,1,2.24,8.57v.1A17.67,17.67,0,0,1,32.22,28a20.12,20.12,0,0,1-1.39,1.72,16.89,16.89,0,0,1-1.64,1.56,16.16,16.16,0,0,1-2.82,1.94,17.33,17.33,0,0,1-8.57,2.24h-.1A17.67,17.67,0,0,1,7.46,32.22a18.56,18.56,0,0,1-1.71-1.39,16.14,16.14,0,0,1-1.57-1.64,16.75,16.75,0,0,1-1.94-2.82A17.44,17.44,0,0,1,0,17.8v-.1A17.61,17.61,0,0,1,3.28,7.46,18.44,18.44,0,0,1,4.66,5.75,17.31,17.31,0,0,1,6.3,4.18,16.35,16.35,0,0,1,9.13,2.24,17.38,17.38,0,0,1,17.7,0ZM8,12.45V23.62a4,4,0,0,0,.42,2,3.16,3.16,0,0,0,.71,1,3.7,3.7,0,0,0,2.62,1H23.54a3.71,3.71,0,0,0,3.76-3.9V12.3a3.93,3.93,0,0,0-.68-2.37,3.65,3.65,0,0,0-1.21-1.06,3.47,3.47,0,0,0-1.81-.5H11.72A3.64,3.64,0,0,0,8.85,9.76a3.59,3.59,0,0,0-.76,1.58A5.77,5.77,0,0,0,8,12.45ZM12.73,16a0,0,0,0,1,0,0,5.14,5.14,0,0,0-.38,2,5.25,5.25,0,0,0,5.17,5.27h.22A5.18,5.18,0,0,0,21.1,22a5.46,5.46,0,0,0,.83-.91,5.18,5.18,0,0,0,1-3V18A5.5,5.5,0,0,0,22.56,16h2.85a0,0,0,0,1,0,0V24a1.83,1.83,0,0,1-.89,1.51,2.1,2.1,0,0,1-1.21.28H12a2,2,0,0,1-1.29-.33c-.05,0-.2-.13-.45-.4A1.84,1.84,0,0,1,9.88,24V16.06l0,0Zm4.84-1.41h.18a3.38,3.38,0,0,1,2.47,1.16,3.47,3.47,0,0,1,.83,2.29,3.4,3.4,0,0,1-6,2.15,3.41,3.41,0,0,1,.16-4.57A3.3,3.3,0,0,1,17.57,14.62Zm7.49-4,0,0v3.23s0,0,0,0h-.41l-2.84,0V10.61h0Z"/></g></g></svg>';
    
    return $insta;
}

function pin_svg() {
    $pin = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.49 35.49"><defs><style>.cls-1{fill:#d2a339;}</style></defs><title>Asset 8</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M17.75,0A17.75,17.75,0,1,0,35.49,17.75,17.75,17.75,0,0,0,17.75,0Zm1.43,23.76c-1.34-.1-1.91-.76-3-1.4-.58,3-1.28,5.94-3.38,7.46-.64-4.59.95-8,1.69-11.69-1.26-2.12.15-6.4,2.82-5.35,3.28,1.3-2.84,7.91,1.27,8.73,4.28.86,6-7.44,3.37-10.14-3.84-3.89-11.18-.08-10.27,5.49.22,1.37,1.63,1.78.56,3.67C9.82,20,9.09,18,9.18,15.46c.15-4.23,3.8-7.2,7.46-7.61,4.63-.51,9,1.7,9.58,6.06C26.89,18.82,24.13,24.15,19.18,23.76Z"/></g></g></svg>';
    
    return $pin;
}

/************* DISABLE EMOJICONS *************/
function ew_disable_wp_emojicons() {
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  add_filter( 'tiny_mce_plugins', 'ew_disable_emojicons_tinymce' );
}
add_action( 'init', 'ew_disable_wp_emojicons' );

/************* DISABLE EMOJICONS TINYMCE *************/
function ew_disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
	return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
	return array();
  }
}

/************* CUSTOM LOGIN LOGO *************/
function ew_login_logo() {
	echo '<style type="text/css">h1 a {background-image:url('.get_template_directory_uri().'/assets/images/login-logo.png) !important;width:280px !important;-webkit-background-size:280px !important; background-size:280px !important;background-position: center center !important;}</style>';
}
add_action('login_head', 'ew_login_logo');

/************* ACF WRAPPER STYLES *************/
function wp_custom_wp_admin_style() {
        wp_register_style( 'acf_admin_css', get_template_directory_uri() . '/assets/css/acf-style.css', false, filemtime(get_stylesheet_directory() . '/assets/css/acf-style.css'));
        wp_enqueue_style( 'acf_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'wp_custom_wp_admin_style' );

/************* CUSTOM POST TYPES *************/
/*
// (http://codex.wordpress.org/Function_Reference/register_post_type)
*/
function ew_venues() {
	register_post_type( 'venue',
		array( 'labels' => array(
			'name' => __( 'Venues' ),
			'singular_name' => __( 'Venue' ),
			'all_items' => __( 'All items' ),
			'add_new' => __( 'Add New' ),
			'add_new_item' => __( 'Add New' ),
			'edit' => __( 'Edit' ),
			'edit_item' => __( 'Edit' ),
			'new_item' => __( 'New' ),
			'view_item' => __( 'View' ),
			'search_items' => __( 'Search' ),
			'not_found' =>  __( 'Nothing found in the Database.' ),
			'not_found_in_trash' => __( 'Nothing found in Trash' ),
			'parent_item_colon' => ''
			),
			'description' => __( 'Venues' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7,
			'menu_icon' => 'dashicons-admin-multisite', // https://developer.wordpress.org/resource/dashicons/
			'rewrite'	=> array( 'slug' => 'our-wedding-venues', 'with_front' => true ),
			'has_archive' => false,
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions', 'sticky', 'page-attributes')
		)
	);

//	register_taxonomy_for_object_type( 'category', 'ew_enquiry' );
//	register_taxonomy_for_object_type( 'post_tag', 'ew_enquiry' );

}
// adding the function to the Wordpress init
//add_action( 'init', 'ew_venues', 0);

/* GMAP KEY */
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyDhTfSFlS_fIgnxw3Us6O9KSJmebE1uql4'); /* API Key */
}
add_action('acf/init', 'my_acf_init');

/************* CHANGE DASHBOARD FOOTER MESSAGE *************/
function ew_change_admin_footer () {
	echo 'Designed &amp; Developed by <a href="https://eightwire.uk" target="_blank">Eight Wire</a>';
}
add_filter('admin_footer_text', 'ew_change_admin_footer');


/************* REMOVE DASHBOARD SUBMENUS *************/
function ew_remove_sub_menus() {
	remove_menu_page('edit-comments.php'); //Comments
	//Remove editor capabilities
	if (current_user_can('editor')) {
		remove_menu_page('tools.php'); //'Tools'
		global $submenu;
		unset($submenu['themes.php'][5]); //'Themes'.
		unset($submenu['customize.php'][1]); //'Themes'.
		unset($submenu['themes.php'][6]); //'Customize'
		unset($submenu['options-general.php'][30]); //'Customize'.
		unset($submenu['options-general.php'][40]); //'Customize'.
	}
}
add_action('admin_menu', 'ew_remove_sub_menus');


/************* REMOVE DASHBOARD METABOXES *************/ //CHECK? - Doesn't seem to be working anyway
function ew_remove_dashboard_meta() {
  remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
  remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
  remove_meta_box('dashboard_primary', 'dashboard', 'side');
  remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
  remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
  remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
  remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'ew_remove_dashboard_meta');


/************* RENAME DEFAULT POSTS TO NEWS *************/
function ew_change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Inspirations';
	$submenu['edit.php'][5][0] = 'All Inspirations';
	$submenu['edit.php'][10][0] = 'Add Inspiration';
	//$submenu['edit.php'][16][0] = 'Tags';
	echo '';
}
add_action('admin_menu', 'ew_change_post_menu_label');

function ew_change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Inspirations';
	$labels->singular_name = 'Inspiration';
	$labels->add_new = 'Add Inspiration';
	$labels->add_new_item = 'Add Inspiration';
	$labels->edit_item = 'Edit Inspiration';
	$labels->new_item = 'Inspiration';
	$labels->view_item = 'View Inspiration';
	$labels->search_items = 'Search Inspirations';
	$labels->not_found = 'No Inspirations found';
	$labels->not_found_in_trash = 'No Inspirations found in Trash';
}
add_action('init', 'ew_change_post_object_label');


/************* ENABLE CATEGORY SLUG SPECIFIC TEMPLATES *************/
//Gets post cat slug and looks for single-[cat-slug].php and applies it
add_filter('single_template', create_function(
	'$the_template',
	'foreach((array) get_the_category() as $cat) {
		if (file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php")) {
			return TEMPLATEPATH . "/single-{$cat->slug}.php";
		}
	}
	return $the_template;'
));


/************* ENABLE PDF AS VALID MEDIA ITEM *************/
function ew_modify_post_mime_types($post_mime_types) {
  $post_mime_types['application/pdf'] = array(__('PDF'), __('Manage PDF'), _n_noop('PDF <span class="count">(%s)</span>', 'PDF <span class="count">(%s)</span>'));
  return $post_mime_types;
}
add_filter('post_mime_types', 'ew_modify_post_mime_types');


/************* HIDE ACF FROM NON-ADMINS *************/
function ew_acf_show_admin($show) {
  return current_user_can('manage_options');
}
add_filter('acf/settings/show_admin', 'ew_acf_show_admin');


/************* ADD THEME SUPPORT FOR $content_width - NECESSARY? *************/
if (!isset($content_width)) { //CHECK?
	$content_width = 960;
}


/************* ADD IMAGE SIZES TO DASHBOARD *************/
/*Make image sizes selectable from the dashboard*/
 function ew_custom_image_sizes($sizes) {

   return array_merge($sizes, array(
       'image' => __('Image'),
   ));

 }
 add_filter('image_size_names_choose', 'ew_custom_image_sizes');


/************* EXTRA BODY CLASSES *************/
function ew_body_class($classes) {

	global $post;
	if (isset($post)) {
		//$current_site = get_current_site();
		//$csite = $current_site->domain;
		$csite = $_SERVER['HTTP_HOST'];
		$csite = explode(".", $csite);
		// $ccat = get_the_category();
		// $thiscat = $ccat[0]->cat_name; //Throwing errors
		// $thiscat = strtolower($thiscat);
		$classs = $post->post_type . ' ' . $post->post_name;
		$classes[] = $csite[0] . ' ' . $classs;
	}
	return $classes;

}
add_filter('body_class','ew_body_class');

/* CUSTOM PAGINATION */
//http://callmenick.com/post/custom-wordpress-loop-with-pagination
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   *
   * It's good because we can now override default pagination
   * in our theme, and use this function in default queries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /**
   * We construct the pagination arguments to enter into our paginate_links
   * function.
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}


//Helper function to duplicate posts - not to be active on live site
/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function rd_duplicate_post_as_draft() {
	global $wpdb;
	if (!(isset($_GET['post']) || isset($_POST['post'])  || (isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action']))) {
		wp_die('No post to duplicate has been supplied!');
	}

	/*
	 * Nonce verification
	 */
	if (!isset($_GET['duplicate_nonce']) || !wp_verify_nonce($_GET['duplicate_nonce'], basename(__FILE__)))
		return;

	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? absint($_GET['post']) : absint($_POST['post']));
	/*
	 * and all the original post data then
	 */
	$post = get_post($post_id);

	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;

	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset($post) && $post != null) {

		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);

		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );

		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}

		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}

		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect(admin_url('post.php?action=edit&post=' . $new_post_id));
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action('admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft');

/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link($actions, $post) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce') . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
add_filter('post_row_actions', 'rd_duplicate_post_link', 10, 2);

function ew_search_form($form) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="'.home_url('/').'">
    <div><label class="screen-reader-text" for="s">'.__('Search for:').'</label>
    <input type="text" value="'.get_search_query().'" name="s" id="s" placeholder="SEARCH ITEMS" />
    <input type="submit" id="searchsubmit" value="" />
    </div>
    </form>';
    
    return $form;
}

add_filter('get_search_form', 'ew_search_form', 100);

function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

/**
 * Replace the woo breadcrumb home link URL
 */
add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );
function woo_custom_breadrumb_home_url() {
    return '/new-in/';
}

function ew_recently_viewed_products($columns=4) {
 
   $viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', wp_unslash( $_COOKIE['woocommerce_recently_viewed'] ) ) : array();
   $viewed_products = array_reverse( array_filter( array_map( 'absint', $viewed_products ) ) );
   if ( empty( $viewed_products ) ) return;
    
   $title = '<div class="page-banner"><h3>Recently Viewed Products<img class="banner-leaf" src="/wp-content/themes/eightwire/assets/images/leaf.svg"></h3></div>';
   $product_ids = implode( ",", $viewed_products );
 
   return $title . '<div class="page-content">' . do_shortcode('[products ids="'.$product_ids.'" columns="'.$columns.'" limit="'.$columns.'"]') . '</div>';
   
}

add_action( 'woocommerce_single_product_summary', 'sold_by', 6 );
add_action( 'woocommerce_after_shop_loop_item_title','sold_by', 3 );
function sold_by(){
        global $product;
        $seller = get_post_field( 'post_author', $product->get_id());
        $author  = get_user_by( 'id', $seller );
        $vendor = dokan()->vendor->get( $seller );
		$store_info = dokan_get_store_info( $author->ID );
        if ( !empty( $store_info['store_name'] ) ) { ?>
                <span class="shopname"><a href="<?php echo $vendor->get_shop_url();?>">
					<?php echo $vendor->get_shop_name(); ?>
		</a>
                </span>
        <?php 
    } 
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_filter('woocommerce_single_product_carousel_options', 'ud_update_woo_flexslider_options');
function ud_update_woo_flexslider_options($options) {
      // show arrows
      $options['directionNav'] = true;
      $options['animation'] = "fade"; //slide is buggy
      $options['smoothHeight'] = "true";

      // infinite loop
      $options['animationLoop'] = false;

      // autoplay (work with only slideshow too)
      $options['slideshow'] = true;
      $options['autoplay'] = false;

      // control nav text (boolean) or thumbnails
      //$options['controlNav'] = true;
      $options['controlNav'] = "thumbnails";
      $options['itemMargin'] = 10;

      $options['mousewheel'] = false;

      return $options;
  }

// Add back to store button on WooCommerce cart page
//add_action('woocommerce_cart_coupon', 'themeprefix_back_to_store');
//add_action( 'woocommerce_cart_actions', 'themeprefix_back_to_store' );
add_action( 'woocommerce_after_cart_table', 'themeprefix_back_to_store' );
function themeprefix_back_to_store() {
echo '<a class="button wc-backward" href="/new-in/">Continue shopping</a>';
}

add_action('init', function() {
    register_taxonomy('product_tag', 'product', [
        'public'            => false,
        'show_ui'           => false,
        'show_admin_column' => false,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false,
    ]);
}, 100);

add_action( 'admin_init' , function() {
    add_filter('manage_product_posts_columns', function($columns) {
        unset($columns['product_tag']);
        return $columns;
    }, 100);
});

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment', 10, 1 );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
	ob_start();
    echo '<a class="basket-btn" href="'.wc_get_cart_url().'"><img src="'.get_template_directory_uri().'/assets/images/basket-i.svg" />SHOPPING BASKET (' . WC()->cart->get_cart_contents_count() . ')</a>';
    $fragments['a.basket-btn'] = ob_get_clean();
    return $fragments;
}

function SearchFilter($query) {
  if ($query->is_search) {
    $query->set('post_type', 'product');
  }
  return $query;
}
//add_filter('pre_get_posts','SearchFilter');

function product_tag_taxonomy() {
	$labels = array(
		'name' => _x( 'Ethics Tags', 'taxonomy general name' ),
		'singular_name' => _x( 'Ethics Tag', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Ethics Tags' ),
		'all_items' => __( 'All Ethics Tags' ),
		'parent_item' => __( 'Parent Ethics Tag' ),
		'parent_item_colon' => __( 'Parent Ethics Tag:' ),
		'edit_item' => __( 'Edit Ethics Tag' ), 
		'update_item' => __( 'Update Ethics Tag' ),
		'add_new_item' => __( 'Add New Ethics Tag' ),
		'new_item_name' => __( 'New Ethics Tag Name' ),
		'menu_name' => __( 'Ethics Tags' ),
	);

	register_taxonomy('ethics', array('product'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'ethics'),
	));
}
add_action('init', 'product_tag_taxonomy', 0);

add_filter( 'woocommerce_product_subcategories_hide_empty', 'hide_empty_categories', 10, 1 );
function hide_empty_categories ( $hide_empty ) {
    $hide_empty  =  FALSE;
    // You can add other logic here too
    return $hide_empty;
}

/* DON'T DELETE THIS CLOSING TAG */ ?>