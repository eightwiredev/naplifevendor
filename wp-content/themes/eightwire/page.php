<?php get_header(); ?>
<main class="main" role="main">
  <article class="article">
        <div class="wrapper cf">
            <?php get_template_part('inc/block', 'page-banner');?>
            <div class="page-content">
                <?php the_content(); ?>
            </div>
        </div>
    </article>
</main>
<?php get_footer(); ?>
