<?php get_header(); ?>
<main class="main" role="main">
    <?php if (have_posts()) { ?>
      <article class="article">
  <div class="wrapper">
        <h1 class="heading">Archive</h1>
        <?php while (have_posts()) {
          the_post(); ?>

          <div class="individual-entry">
            <?php if (has_post_thumbnail()) { ?>
              <a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail('single'); ?></a>
            <?php } ?>
            <a href="<?php echo get_permalink(); ?>"><h2><?php echo get_the_title(); ?></h2></a>
            <br>
            <?php the_excerpt(); ?>
          </div>

        <?php } ?>
  </div>
      </article>
    <?php } else { ?>
      <article class="article">
        <h1 class="heading">Not Found</h1>
      </article>
    <?php } ?>
</main>
<?php get_footer(); ?>
