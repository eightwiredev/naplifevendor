<?php get_header(); ?>
<main class="main" role="main">
    <article class="article">
        <div class="wrapper cf">
            <?php get_template_part('inc/block', 'post-banner');?>
            <?php
            $banner_img = get_field('banner_image');
            echo '<img src="'.$banner_img["sizes"]["insp-banner"].'" />';
            ?>
            <?php get_template_part('inc/block', 'layout');?>
            <div class="custom-pagination cf">
                <div class="alignleft">
                    <?php next_post_link('%link','< Previous'); ?>
                </div>
                <div class="alignright">
                    <?php previous_post_link('%link','Next >'); ?>
                </div>
            </div>
            <?php get_template_part('inc/block', 'product-banner');?>
        </div>
    </article>
</main>
<?php get_footer(); ?>