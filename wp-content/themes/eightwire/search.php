<?php get_header(); ?>
<main class="main" role="main">
<?php get_template_part('inc/block', 'page-banner');?>
<?php if (have_posts()) { ?>
  <article class="article">
        <div class="wrapper cf">
            <div class="page-content">
			<div class="product-banner-content flex flexwrap">
        <?php 
		while (have_posts()) {
          the_post();
		   ?>
<div class="product-banner-product" <?php if (!has_post_thumbnail()) { echo 'style="flex-basis:100%;padding:1em 0;"'; } ?>>
<a href="<?php echo get_permalink(); ?>">
            <?php if (has_post_thumbnail()) { 
              echo '<div class="product-img">';
			  the_post_thumbnail('single'); 
			  echo '<div class="pi-overlay flex">';
                                                echo '<div class="pi-overlay-square">';
                                                    echo '<p>+</p>';
                                                echo '</div>';
                                            echo '</div>';
			  echo '</div>'; }
			echo '<h4>'.get_the_title().'</h4>';
			if (isset($product)) {
                                        $vendor_id = get_post_field('post_author', get_the_ID());
                                        $vendor_info = dokan_get_store_info($vendor_id);
                                        $vendor_name = $vendor_info['store_name'];
                                        echo '<h5>'.$vendor_name.'</h5>';
                                        echo '<p>'.wc_price(wc_get_price_to_display($product, array('price' => $product->get_price()))).'</p>';
										}
                                        echo '<hr>';
			?>
			</a>
          </div>

        <?php }
		?>
		<div>
  </div>
  </div>
      </article>
    <?php } else { ?>
      <article class="article">
	  <div class="wrapper cf">
	  <div class="page-content">
        <p>Not Found</p>
		</div>
		</div>
      </article>
    <?php } ?>
</main>
<?php get_footer(); ?>
