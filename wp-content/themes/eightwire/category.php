<?php get_header(); ?>
<main class="main" role="main">
<h1>News</h1>
<article class="news-section cf">
<div class="wrapper">
<?php
// WP_Query arguments
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$args = array(
	'post_type'              => array( 'post' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '2',
	'order'                  => 'DESC',
	'orderby'                => 'date',
	'paged' => $paged,
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) { ?>
<?php
	while ( $query->have_posts() ) {
		$query->the_post();
		echo '<div class="news-article cf">';
		if (has_post_thumbnail()) {
              echo '<a href="'.get_permalink().'" class="post-thumb">'.get_the_post_thumbnail($post_id, 'single').'</a>';
        }
		echo '<div class="news-article-content">';
		echo '<h2>'.get_the_title().'</h2>';
		echo 'Posted on: '.get_the_date().'<br><br>';
		the_excerpt();
		echo '<a href="'.get_permalink().'" class="read-more">READ MORE ></a>';
		echo '</div>';
		echo '</div>';
	}
	if (function_exists(custom_pagination)) {
        custom_pagination($query->max_num_pages,"",$paged);
    } 
	wp_reset_postdata();
} else { 
	// no posts found ?>
<h2>No news</h2>
<?php
}

// Restore original Post Data
wp_reset_postdata();
?>
</div>
</article>		
</main>
<?php get_footer(); ?>
