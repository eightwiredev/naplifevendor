<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>
<main class="main" role="main">
  <?php if (have_posts()) { ?>
    <article class="article">
        <?php while (have_posts()) {
          the_post(); 
		  ?>
      <div class="wrapper cf">
          <h2><?php the_title(); ?></h2>
          <section class="content"><?php the_content(); ?></section>
        </div>

<section class="contactinfo">
    <div>
              <p><strong>Please find all of our contact details below, for enquiries please call or email us</strong></p>
              <p>Business,<br>
              Street address,<br>
              Cornwall<br>
              POST CODE</p>
             <p><a href="tel:+441234567890">01234 567890</a></p>
              <p><a href="mailto:info@domain.com">info@domain.com</a></p>
                <div class="social">
                  <a href="<?php echo get_field('facebook','option'); ?>" target="_blank"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/icon-facebook.svg");?></a>
                  <a href="<?php echo get_field('twitter','option'); ?>" target="_blank"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/icon-twitter.svg");?></a>
                  <a href="<?php echo get_field('instagram','option'); ?>" target="_blank"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/icon-instagram.svg");?></a>
                  <a href="<?php echo get_field('youtube','option'); ?>" target="_blank"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/icon-youtube.svg");?></a>
              </div> 
            </div>        
   <div class="gmap">
        <div>  
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhTfSFlS_fIgnxw3Us6O9KSJmebE1uql4"></script>
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 15,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 15 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>

        </div>
<?php 

$location = get_field('map');

if( !empty($location) ):
?>
<div class="acf-map">
	<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
</div>
<?php endif; ?>     </div>         
  </section>
	<?php get_template_part('inc/block', 'panels'); ?>
        <?php } ?>
    </article>
  <?php } else { ?>
    <article class="article">
    </article>
  <?php } ?>
</main>
<?php get_footer(); ?>
