<?php
// WP_Query arguments
$args = array(
	'post_type'              => array( 'post' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '2',
	'order'                  => 'DESC',
	'orderby'                => 'date',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) { ?>
<section class="news-section cf">
<div class="wrapper cf">
<h2>Latest news</h2>
<?php
	while ( $query->have_posts() ) {
		$query->the_post();
		echo '<div class="news-article cf">';
		if (has_post_thumbnail()) {
			$post_id=get_the_ID();
              echo '<a href="'.get_permalink().'" class="post-thumb">'.get_the_post_thumbnail($post_id, 'single').'</a>';
        }
		echo '<div class="news-article-content">';
		echo '<h3>'.get_the_title().'</h3>';
		echo 'Posted on: '.get_the_date().'<br><br>';
		the_excerpt();
		echo '<a href="'.get_permalink().'" class="read-more">READ MORE ></a>';
		echo '</div>';
		echo '</div>';
	} ?>
</div>
</section>
<?php
}
wp_reset_postdata();
?>