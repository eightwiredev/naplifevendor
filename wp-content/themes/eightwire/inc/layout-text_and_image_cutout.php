<div class="cutout-wrapper">
    <div class="cutout-content flex flexwrap">
        <?php
            $header = get_sub_field('header');
            $text = get_sub_field('text');
            $image = get_sub_field('image');
        
            echo '<div class="cutout-image" style="background-image:url('.$image["sizes"]["media"].')"></div>';
            
            echo '<div class="cutout-text">';
                echo '<h3>'.$header.'<img class="cutout-leaf" src="'.get_template_directory_uri().'/assets/images/leaf.svg" /></h3>';
                echo '<hr class="blue-hr">';
                echo '<p>'.$text.'</p>';
            echo '</div>';
        ?>
    </div>
</div>