<?php 	
if( have_rows( 'researchers' ) ): ?>
<div class="wrapper cf">
	<section class="researchers cf">
    
    <h2><?php 
	if (wp_get_post_parent_id($post_ID)==0){
		echo 'Themes';
	}else{
		echo 'Researchers';
		$target=' target="_blank"';
	}; 
	?></h2>
    
		<div class="flex flexwrap">

	<?php 
		while( have_rows( 'researchers' ) ): the_row(); 

		$image = get_sub_field('image');
		$name = get_sub_field('name');
		$link = get_sub_field('link');

		if ( $link["url"] =='' ){ $link["url"] = '#'; }
	
		?>

		<div class="researcher flexone">

		    <?php echo '<a href="'.$link["url"].'"'.$target.'>'; ?>
			<?php echo '<img src="'.$image["sizes"]["square"].'" alt="" />'; ?><br>
		    <?php echo $name; ?>
		    <?php echo '</a>'; ?>

		</div>

	<?php 
	endwhile;
	 ?>
 
		</div>
 
	</section>
		</div>
<?php endif; ?>