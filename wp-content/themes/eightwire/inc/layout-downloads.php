<?php if( have_rows('files') ): ?>
<div class="wrapper cf">
	<section class="downloads cf">
    
    <h2>More information</h2>
    
		<div class="flex flexwrap">

	<?php while( have_rows('files') ): the_row(); 

		$title = get_sub_field('title');
		$file = get_sub_field('file');

		?>

		<div class="download flexone">

		    <?php echo '<a href="'.$file["url"].'" target="_blank" class="btn">'.$title.'</a>'; ?>

		</div>

	<?php endwhile; ?>
 
		</div>
 
	</section>
    
		</div>

<?php endif; ?>