<div class="get-inspired-container">
    <div class="get-inspired-banner flex">
        <h4>Be inspired<img src="<?php echo get_template_directory_uri();?>/assets/images/leaf.svg" /></h4>
    </div>
    <div class="get-inspired-content flex flexwrap">
        <?php
        $args = array(
            'post_type'              => array( 'post' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => '2',
            'order'                  => 'DESC',
            'orderby'                => 'date',
        );

        // The Query
        $query = new WP_Query( $args );

        // The Loop
        if ( $query->have_posts() ) { ?>
        <?php
            while ( $query->have_posts() ) {
                $query->the_post();
                echo '<div class="get-inspired-preview">';
                        echo '<div class="gi-preview-content flex flexwrap">';
                        echo '<div class="gi-img">';
                            echo '<a href="'.get_the_permalink().'">';
                                echo '<img src="'.get_the_post_thumbnail_url(get_the_ID(), 'gi-image').'" />';
                                echo '<div class="pi-overlay flex">';
                                    echo '<div class="pi-overlay-square">';
                                        echo '<p>+</p>';                         echo '</div>';
                                echo '</div>';
                            echo '</a>';
                        echo '</div>';
                        echo '<div class="gi-text">';
                            echo '<h5>'.get_the_title().'</h5>';
                            echo '<h6>'.get_the_date('d.m.y').'</h6>';
                            echo '<hr>';
                            echo '<p>'.get_the_excerpt().'</p>';
                            echo '<a href="'.get_the_permalink().'">SEE MORE</a>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
            }
            wp_reset_postdata();
        } 
        // Restore original Post Data
        wp_reset_postdata();
        ?>
    </div>
    <a class="insp-btn" href="/inspiration">SEE MORE</a>
</div>