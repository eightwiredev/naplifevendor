<?php
$search_val = '';
    if (isset($_GET["search"])) {
        $search_val = $_GET["search"];
    }

$cat_val = '';
if (isset($_GET["cat"])) {
    $cat_val = $_GET["cat"];
}

?>

<div class="inspo-wrapper">
    <div class="inspo-filtering">
        <form method="get" action="/inspiration" class="flex flexwrap">
            <input type="text" name="search" id="search" placeholder="SEARCH INSPIRATION" />
            <select name="cat" class="cat-select">
                <option value="" selected>Category Filter</option>
                <option value="">All Ideas</option>
                <?php
                $term_args = array(
                    'type' => 'post',
                    'hide_empty' => false,
                );
                $terms = get_categories($term_args);
                foreach ($terms as $term) {
                    echo '<option value="'.$term->slug.'"';
                    if ($cat_val == $term->slug)
                        echo ' selected';
                    echo '>'.$term->name.'</option>';
                }
                ?>
            </select>
            <input type="submit" value="Search" class="insp-submit" />
        </form>
        
    </div>
    <?php
    
    
    // WP_Query arguments
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    
    if ($cat_val && $search_val) {
        $args = array(
            'post_type'              => array( 'post' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => '6',
            'order'                  => 'DESC',
            'orderby'                => 'date',
            'paged' => $paged,
            's' => $search_val,
            'category_name' => $cat_val,
        );
    } else if ($search_val) {
        $args = array(
            'post_type'              => array( 'post' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => '6',
            'order'                  => 'DESC',
            'orderby'                => 'date',
            'paged' => $paged,
            's' => $search_val,
        );
    } else if ($cat_val) {
        $args = array(
            'post_type'              => array( 'post' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => '6',
            'order'                  => 'DESC',
            'orderby'                => 'date',
            'paged' => $paged,
            'category_name' => $cat_val,
        );
    }  else {
    
        $args = array(
            'post_type'              => array( 'post' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => '6',
            'order'                  => 'DESC',
            'orderby'                => 'date',
            'paged' => $paged,
        );
    }

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) { ?>
    <div class="inspo-previews flex flexwrap">
    <?php
        while ( $query->have_posts() ) {
            $query->the_post();
            echo '<div class="get-inspired-preview">';
                    echo '<div class="gi-preview-content flex flexwrap">';
                    echo '<div class="gi-img">';
                            echo '<a href="'.get_the_permalink().'">';
                                echo '<img src="'.get_the_post_thumbnail_url(get_the_ID(), 'gi-image').'" />';
                                echo '<div class="pi-overlay flex">';
                                    echo '<div class="pi-overlay-square">';
                                        echo '<p>+</p>';                         echo '</div>';
                                echo '</div>';
                            echo '</a>';
                        echo '</div>';
                    echo '<div class="gi-text">';
                        echo '<h5>'.get_the_title().'</h5>';
                        echo '<h6>'.get_the_date('d.m.y').'</h6>';
                        echo '<hr>';
                        $post_categories = get_post_primary_category($post->id, 'category');
                        $primary_category = $post_categories['primary_category'];
                        echo '<p class="cat">';
                        echo $primary_category->name;
                        echo '</p>';
                        echo '<a href="'.get_the_permalink().'">SEE MORE</a>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        }
        wp_reset_postdata();
        
    echo '</div>';
    } 
$big = 999999999; //Need an unlikely integer

$pagination_html = paginate_links(array(
 'prev_text' => '< Prev',
 'next_text' => 'Next >',
 'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
 'format' => '?paged=%#%',
 'current' => max(1, $paged),
 'total' => $query->max_num_pages,
    'prev_next' => false,
));
?>

<?php if ($pagination_html) { ?>

<div class="pagination-archive">
 <?php echo $pagination_html; ?>
</div>

<?php }  
    
    if (!$query->have_posts()){ 
        // no posts found ?>
    <h2 class="np">No posts found</h2>
    <?php
    }

    // Restore original Post Data
    wp_reset_postdata();
    ?>
</div>
    
<script type="text/javascript">
    jQuery('.cat-select').change(function() {
         jQuery('.insp-submit').click();
    });
</script>