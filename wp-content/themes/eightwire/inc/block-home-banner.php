<?php
    $video = get_field('video');
    
    if ($video) {
    echo '<div class="home-banner">';
        echo $video;
    echo '</div>';
    } else {
        echo '<div>';
            echo '<img src="'.get_template_directory_uri().'/assets/images/home-banner.png" style="margin: 0 auto;" />';
        echo '</div>';
    }
?>
<?php
/*$image = get_field('banner_image');
$text = get_field('banner_text');
$link = get_field('banner_link');

if ($link) {
    echo '<a class="hb-link" href="'.$link["url"].'">';
} 
echo '<div class="home-banner-container" style="background-image: url('.$image["sizes"]["banner"].');">';
    echo '<img class="hb-img" src="'.$image["sizes"]["banner"].'" />';
    echo '<div class="home-banner-content flex">';
        echo '<div class="home-banner-text">';
            echo '<h2>'.$text.'</h2>';
            echo '<img src="'.get_template_directory_uri().'/assets/images/leaf.svg" />';
        echo '</div>';
    echo '</div>';
    
echo '</div>';
if ($link) {
    echo '</a>';
}
*/?>