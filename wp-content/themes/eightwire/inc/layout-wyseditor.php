<?php 
$wystitle=get_sub_field('wystitle');
$wyseditor=get_sub_field('wyseditor');
?>
<section class="wyseditor">
    <?php 
        if ($wystitle):
            echo '<h2>'.$wystitle,'</h2>';
        endif;
        if ($wyseditor):
            echo '<section class="wyscontent">'.$wyseditor.'</section>';
        endif; 
		?>
</section>