<section class="media">
<?php
$vid=get_field('vid');//pS_HACD4ENg
$pic=get_field('pic');
if ($vid) {
	//vid
	echo '<div class="ratio">';
	echo '<iframe src="//www.youtube.com/embed/'.$vid.'?rel=0&amp;autoplay=1&amp;modestbranding=1&amp;iframe=true&amp;allowfullscreen=true" scrolling="no" allowfullscreen="yes"></iframe>';
	echo '</div>';
}else{
	if ($pic) {
		//pic
		echo '<img src="'.$pic[sizes]["media"].'" />';
	}else{
		//fallback
		echo '<img src="'.get_template_directory_uri().'"/assets/images/vid.png" />';
	}
}
?>
</section>