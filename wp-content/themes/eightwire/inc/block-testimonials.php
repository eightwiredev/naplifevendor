<?php 
$trows = get_field('testimonials','option'); 
shuffle ($trows); //randomize rows
if($trows) { ?>
	<section class="testimonials cf">
        <div class="flexslider cf">
            <h3>TESTIMONIALS</h3>
            <div class="slides">
			<?php foreach($trows as $trow) { ?>
                <div class="slide">
                    <p><?php echo $trow['quote']; ?></p>
                </div>
			<?php } ?>
            </div>
        </div>
	</section>
<?php } ?>