<?php
wp_reset_postdata();
// WP_Query arguments
$args = array(
	'post_type'              => array( 'venue' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => -1,
	'order'                  => 'ASC',
	'orderby'                => 'menu_order',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) { ?>
<section class="venues-section">
        <div class="wrapper">
	<div class="venues-panels">
<?php
	while ( $query->have_posts() ) {
		$query->the_post();
		?>
        <div class="venues-panel">
        	<div class="venues-panel-inner" style="background-image:url(<?php echo get_field('venue_image')['sizes']['panel'];?>);">
                <div class="venues-panel-pre">
                    <h4><?php the_title();?></h4>
                    <?php the_field("venue_bullets");?>
                    <a href="<?php the_permalink();?>">SEE MORE</a>
                </div>
            </div>
        </div>
<?php
	} ?>
        <div class="venues-panel">
        	<div class="venues-panel-inner">
                <div class="venues-panel-post">
            	<h4>Have you got a beautiful wedding venue?</h4>
                <p>If you have a beautiful building or stunning grounds in the Midlands or South West which are the perfect setting for a wedding, contact Country & Coast Wedding to see if we can help. We look after all the planning and management required when hosting weddings and occasions.</p>
				<a href="<?php the_permalink(167);?>">CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
    <a href="<?php the_permalink(99);?>" class="venues-cta"><h5><span>CLICK HERE TO&nbsp;</span>SEE MORE VENUES</h5><hr /></a>
    </div>
</section>
<?php
}
wp_reset_postdata();
?>