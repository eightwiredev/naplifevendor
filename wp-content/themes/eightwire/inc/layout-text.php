<?php 
$title=get_sub_field('title');
$text=get_sub_field('text');
$text2=get_sub_field('text2');
if ($text && $text2) { $cols = 'cols '; }
echo '<section class="media-text cf">';
	echo '<div class="wrapper cf">';
	echo '<h3>'.$title.'</h3>';
		echo '<div class="'.$cols.'colwrap cf">';
			echo '<div class="col-one">';
			echo $text;
			echo '</div>';
		if ($text2) {
			echo '<div class="col-two">';
			echo $text2;
			echo '</div>';
		}
		echo '</div>';
	echo '</div>';
echo '</section>';
?>