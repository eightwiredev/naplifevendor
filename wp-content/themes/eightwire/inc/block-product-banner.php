<?php
if (have_rows('products_banner')) {
    echo '<div class="product-banners-container">';
        while (have_rows('products_banner')) {
            the_row();
            $header = get_sub_field('banner_header');
            $display = get_sub_field('display');
            $category = get_sub_field('category');
            echo '<div class="product-banner">';
                echo '<div class="product-banner-header flex">';
                    echo '<h3>'.$header.'<img src="'.get_template_directory_uri().'/assets/images/leaf.svg" /></h3>';
                echo '</div>';
                echo '<div class="product-banner-content flex flexwrap">';
                    if ($display == 'popular') {
                        $args = array(
                            'post_type' => 'product',
                            'meta_key' => 'total_sales',
                            'orderby' => 'meta_value_num',
                            'posts_per_page' => 4,
                        );
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()) {
                            while ($loop->have_posts()) {
                                $loop->the_post();
                                global $product;
                                $product = wc_get_product(get_the_ID());
                                echo '<div class="product-banner-product">';
                                    echo '<a href="'.get_the_permalink().'">';
                                        echo '<div class="product-img">';
                                            echo '<img src="'.get_the_post_thumbnail_url(get_the_ID(), 'media').'" />';
                                            echo '<div class="pi-overlay flex">';
                                                echo '<div class="pi-overlay-square">';
                                                    echo '<p>+</p>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '</div>';
                                        echo '<h4>'.get_the_title().'</h4>';
                                        $vendor_id = get_post_field('post_author', get_the_ID());
                                        $vendor_info = dokan_get_store_info($vendor_id);
                                        $vendor_name = $vendor_info['store_name'];
                                        echo '<h5>'.$vendor_name.'</h5>';
                                        echo '<p>'.wc_price(wc_get_price_to_display($product, array('price' => $product->get_price()))).'</p>';
                                        echo '<hr>';
                                    echo '</a>';
                                echo '</div>';
                            }
                            wp_reset_query();
                        }
                    } else if ($display == 'new') {
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => 4,
                            'orderby' => 'date',
                            'order' => 'DESC',
                        );
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()) {
                            while ($loop->have_posts()) {
                                $loop->the_post();
                                global $product;
                                $product = wc_get_product(get_the_ID());
                                echo '<div class="product-banner-product">';
                                    echo '<a href="'.get_the_permalink().'">';
                                        echo '<div class="product-img">';
                                            echo '<img src="'.get_the_post_thumbnail_url(get_the_ID(), 'media').'" />';
                                            echo '<div class="pi-overlay flex">';
                                                echo '<div class="pi-overlay-square">';
                                                    echo '<p>+</p>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '</div>';
                                        echo '<h4>'.get_the_title().'</h4>';
                                        $vendor_id = get_post_field('post_author', get_the_ID());
                                        $vendor_info = dokan_get_store_info($vendor_id);
                                        $vendor_name = $vendor_info['store_name'];
                                        echo '<h5>'.$vendor_name.'</h5>';
                                        echo '<p>'.wc_price(wc_get_price_to_display($product, array('price' => $product->get_price()))).'</p>';
                                        echo '<hr>';
                                    echo '</a>';
                                echo '</div>';
                            }
                            wp_reset_query();
                        }
                    } else if ($display == 'category') {
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => 4,
                            'orderby' => 'rand',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'product_cat',
                                    'field' => 'term_id',
                                    'terms' => $category,
                                ),
                            ),
                        );
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()) {
                            while ($loop->have_posts()) {
                                $loop->the_post();
                                global $product;
                                $product = wc_get_product(get_the_ID());
                                echo '<div class="product-banner-product">';
                                    echo '<a href="'.get_the_permalink().'">';
                                        echo '<div class="product-img">';
                                            echo '<img src="'.get_the_post_thumbnail_url(get_the_ID(), 'media').'" />';
                                            echo '<div class="pi-overlay flex">';
                                                echo '<div class="pi-overlay-square">';
                                                    echo '<p>+</p>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '</div>';
                                        echo '<h4>'.get_the_title().'</h4>';
                                        $vendor_id = get_post_field('post_author', get_the_ID());
                                        $vendor_info = dokan_get_store_info($vendor_id);
                                        $vendor_name = $vendor_info['store_name'];
                                        echo '<h5>'.$vendor_name.'</h5>';
                                        echo '<p>'.wc_price(wc_get_price_to_display($product, array('price' => $product->get_price()))).'</p>';
                                        echo '<hr>';
                                    echo '</a>';
                                echo '</div>';
                            }
                            wp_reset_query();
                        } 
                    } else if ($display == 'custom') {
                        
                        if (have_rows('products')) {
                            while (have_rows('products')) {
                                the_row();
                                $post_object = get_sub_field('product');
                                global $product;
                                $product = wc_get_product($post_object->ID);
                                $post = $post_object;
                                setup_postdata($post);
                                echo '<div class="product-banner-product">';
                                    echo '<a href="'.get_the_permalink().'">';
                                        echo '<div class="product-img">';
                                            echo '<img src="'.get_the_post_thumbnail_url(get_the_ID(), 'media').'" />';
                                            echo '<div class="pi-overlay flex">';
                                                echo '<div class="pi-overlay-square">';
                                                    echo '<p>+</p>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '</div>';
                                        echo '<h4>'.get_the_title().'</h4>';
                                        $vendor_id = get_post_field('post_author', get_the_ID());
                                        $vendor_info = dokan_get_store_info($vendor_id);
                                        $vendor_name = $vendor_info['store_name'];
                                        echo '<h5>'.$vendor_name.'</h5>';
                                        echo '<p>'.wc_price(wc_get_price_to_display($product, array('price' => $product->get_price()))).'</p>';
                                        echo '<hr>';
                                    echo '</a>';
                                echo '</div>';
                                wp_reset_postdata();
                            }
                        }
                    }
                echo '</div>';
            echo '</div>';
        }
    echo '</div>';
}
?>