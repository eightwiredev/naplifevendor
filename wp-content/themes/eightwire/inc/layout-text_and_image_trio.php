<div class="img-trio-wrapper">
    <div class="img-trio-container flex flexwrap">
        <?php
            $header = get_sub_field('header');
            $text = get_sub_field('text');
            
            if (have_rows('images')) {
                echo '<div class="img-trio flex flexwrap">';
                $count = 0;
                while(have_rows('images')) {
                    the_row();
                    $count++;
                    $image = get_sub_field('image');
                    if ($count != 3) {
                        echo '<div style="background-image:url('.$image["sizes"]["media"].')"></div>';
                    } else {
                        echo '<div class="grow" style="background-image:url('.$image["sizes"]["media"].')"></div>';
                    }
                }
                echo '</div>';
            }
        
            echo '<div class="trio-text-holder">';
                echo '<h3>'.$header.'<img class="cutout-leaf" src="'.get_template_directory_uri().'/assets/images/leaf.svg" /></h3>';
                echo '<hr class="blue-hr">';
                echo '<p>'.$text.'</p>';
            echo '</div>';
        ?>
    </div>
</div>