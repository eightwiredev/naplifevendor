<?php //split
$title=get_sub_field('title');
$link=get_sub_field('link');
$photos=get_sub_field('photos');
$secondarytext=get_sub_field('secondary_text');
$maintext=get_sub_field('main_text');
$link2=get_sub_field('link_2');
$title2=get_sub_field('title_2');
$link3=get_sub_field('link_3');
$photos2=get_sub_field('photos_2');
$secondarytext2=get_sub_field('secondary_text_2');
$maintext2=get_sub_field('main_text_2');
$link4=get_sub_field('link_4');
?>
<section class="igrid">
    <div class="i c v"<?php if ($photos):
            echo ' style="background-image:url('.$photos[0]['sizes']['photoz'].');"';
        endif; ?>><div><?php echo '<h1>'.$title.'</h1>';
	if ($link):
            echo '<a href="'.$link["url"].'" target="'.$link["target"].'" class="btn-booknow">'.$link["title"].'</a>';
        endif; ?></div></div>
    <div class="h i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[1]['sizes']['photozz'].');"';
        endif; ?>></div>
    <div class="h s c"><div><?php 
        echo $secondarytext;
        ?></div></div>
    <div class="z equal"><div><?php echo $maintext; if ($link2):
            echo '<a href="'.$link2["url"].'" target="'.$link2["target"].'" class="btn-booknow">'.$link2["title"].'</a>';
        endif; ?></div></div>
</section>
<section class="igrid">
    <div class="i c v"<?php if ($photos2):
            echo ' style="background-image:url('.$photos2[0]['sizes']['photoz'].');"';
        endif; ?>><div><?php echo '<h1>'.$title2.'</h1>';
	if ($link3):
            echo '<a href="'.$link3["url"].'" target="'.$link3["target"].'" class="btn-booknow">'.$link3["title"].'</a>';
        endif; ?></div></div>
    <div class="h i"<?php if ($photos2):
            echo ' style="background-image:url('.$photos2[1]['sizes']['photozz'].');"';
        endif; ?>></div>
    <div class="h s c"><div><?php 
        echo $secondarytext2;
         ?></div></div>
    <div class="z equal"><div><?php echo $maintext2; if ($link4):
            echo '<a href="'.$link4["url"].'" target="'.$link4["target"].'" class="btn-booknow">'.$link4["title"].'</a>';
        endif;?></div></div>
</section>