<div class="image-trio flex flexwrap">
    <?php
    if (have_rows('images')) {
        while (have_rows('images')) {
            the_row();
            $image = get_sub_field('image');
            echo '<div class="it-img">';
                echo '<img src="'.$image["sizes"]["media"].'" />';
            echo '</div>';
        }
    }
    ?>
</div>