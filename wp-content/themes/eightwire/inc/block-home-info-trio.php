<?php
if (have_rows('home_info')) {
    echo '<div class="home-info-container flex flexwrap">';
    while (have_rows('home_info')) {
        the_row();
        $image = get_sub_field('image');
        $text = get_sub_field('text');
        echo '<div class="home-info-col">';
            echo '<div class="home-info-img">';
                echo '<img src="'.$image["sizes"]["media"].'" />';
                echo '<img class="hi-leaf" src="'.get_template_directory_uri().'/assets/images/leaf-2.svg" />';
            echo '</div>';
            echo '<p>'.$text.'</p>';
            echo '<hr>';
        echo '</div>';
    }
    echo '</div>';
}
?>