<div class="newsletter-banner">
    <h4>Sign up to our newsletter</h4>
    <div class="nb-wpcf7">
        <?php echo do_shortcode('[contact-form-7 id="458" title="Newsletter Banner Form"]');?>
        <img src="<?php echo get_template_directory_uri();?>/assets/images/left-leaf.svg" />
    </div>
</div>