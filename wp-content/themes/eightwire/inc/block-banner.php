<?php
	if (function_exists('have_rows')) {
		$parentid='';
		if (is_singular("post")){$parentid=171;}
		if (have_rows('banner',$parentid)) { ?>
		<div class="banner-container">
			<section class="flexslider banner">
				<ul class="slides">
					<?php while( have_rows('banner',$parentid) ) {
						the_row();
						$link = get_sub_field('link');
						$caption = get_sub_field('caption');
						$image = get_sub_field('image');
						$banner_img = $image['sizes']['banner'];
						$focal = get_sub_field('focal');
						if (!$focal){$focal='50% 50%';}
						$youtube = get_sub_field('youtube');
						if ($youtube) :
						?>
                        <li style="background:#A4961E url('/wp-content/themes/eightwire/assets/images/white-bg.png') top center repeat scroll;">
                            <div class="embed-container">
                                     <?php if ($caption) { ?>
                                        <div class="flexslider_caption_wrapper">
                                            <h1 class="flexslider_caption"><?php echo $caption; ?></h1>
                                            <?php if( $link ): ?>
                                                <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                                            <?php endif; ?>
                                        </div>
                                    <?php } ?>
                               <div class="video-bg cover">
                                    <div class="video-fg">
                                        <!--<div id="youtubevideo"></div>-->
                                        <iframe id="youtubevideo" src="https://www.youtube.com/embed/<?php echo $youtube; ?>?rel=0&autohide=1&controls=0&showinfo=0&hd=1&modestbranding=1&iv_load_policy=3&cc_load_policy=0&color=white&iframe=true&enablejsapi=1&allowfullscreen=true&loop=1&playlist=<?php echo $youtube; ?>&list=<?php echo $youtube; ?>&autoplay=1" frameborder="0" allowfullscreen></iframe><!-- enablejsapi=1&origin=domain.tld -->
                                    </div>
                                </div>
                             </div>
                       </li>
                        <?php endif;
						if ($image) : ?>
						<li>
                                <div class="flexslider_background" style="background-image:url('<?php echo $banner_img; ?>');background-position:<?php echo $focal; ?>;">
                                    <?php if ($caption) { ?>
                                        <div class="flexslider_caption_wrapper">
                                            <h1 class="flexslider_caption"><?php echo $caption; ?></h1>
                                            <?php if( $link ): ?>
                                                <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                                            <?php endif; ?>
                                        </div>
                                    <?php } ?>
                                </div>
						</li>
					<?php endif;
					} ?>
				</ul>
			</section>
            <!--<div class="searchbar">
            </div>-->
		</div>
	<?php
		}
	}
?>