<div class="lrg-img-text-wrapper flex flexwrap">
    <?php
        $image = get_sub_field('image');
        $text = get_sub_field('text');
        
        echo '<div class="img">';
            echo '<img src="'.$image["url"].'" />';
        echo '</div>';
        echo '<div class="text">';
            echo '<p>'.$text.'</p>';
        echo '</div>';
    ?>
</div>