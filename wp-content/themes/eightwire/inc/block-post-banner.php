<div class="post-banner">
    <div class="post-banner-header flex">
        <h1><?php echo get_the_title();?><img src="<?php echo get_template_directory_uri();?>/assets/images/leaf.svg" /></h1>
    </div>
    <p class="date"><?php echo get_the_date('d.m.y');?></p>
    <hr>
    <p class="excerpt"><?php echo get_the_excerpt();?></p>
</div>