<div class="text-img-wrapper flex flexwrap">
    <?php
        $text = get_sub_field('text');
        $image = get_sub_field('image');
        $order = get_sub_field('order');
        
        if (empty($order)) {
            echo '<div class="layout-text">';
                echo '<img class="layout-leaf right" src="'.get_template_directory_uri().'/assets/images/leaf-2.svg" />';
                echo '<p>'.$text.'</p>';
            echo '</div>';
            echo '<div class="layout-img">';
                echo '<img src="'.$image["sizes"]["ti-image"].'" />';
            echo '</div>';            
        } else {
            echo '<div class="layout-img">';
                echo '<img src="'.$image["sizes"]["ti-image"].'" />';
            echo '</div>';
            echo '<div class="layout-text">';
                echo '<img class="layout-leaf" src="'.get_template_directory_uri().'/assets/images/leaf-2.svg" />';
                echo '<p>'.$text.'</p>';
            echo '</div>';
        }
    ?>
</div>