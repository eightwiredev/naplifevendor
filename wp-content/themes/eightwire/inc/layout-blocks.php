<?php 
	if (function_exists('have_rows')) {
		if (have_rows('blocks')) { ?>
			<section class="panels blocks cf">
					<?php while( have_rows('blocks') ) {
						the_row();
						$image = get_sub_field('image');
						$title = get_sub_field('title');
						$price = get_sub_field('price');
						$info = get_sub_field('info');
						?>
						<div class="panel"<?php echo ' style="background-image:url('.$image['sizes']['blocks'].');"'; ?>>
                            <div class="overlay">
							<?php
							if ($title) { echo '<p class="block-title">'.$title.'</p>'; } 
							if ($price) { echo '<p class="block-price">'.$price.'</p>'; } 
							if ($info) { echo '<div class="block-info">'.$info.'</div>'; } 
							?>
	                        </div>
                        </div>
					<?php } ?>
        </section>	
<?php
		}
	}
?>
