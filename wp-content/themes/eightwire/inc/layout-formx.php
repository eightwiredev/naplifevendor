<?php 
//ACF form
$whichform = get_sub_field('whichform');
$formtitle = get_sub_field('formtitle');
$formid=3;
if ($whichform=="takepart") { $formid=1; }
if ($whichform=="work")     { $formid=2; }
if ($whichform=="contact")  { $formid=3; }
echo '<div class="wrapper cf">';
	echo '<section id="xform" class="formx cf">';
	echo '<h2>'.$formtitle.'</h2>';
	echo do_shortcode('[acf_contact id="'.$formid.'" submit_value="Send" updated_message="Thank you for your submission"]');
	echo '</section>';
	echo '</div>';
	echo "
	<script>
	function get(n) {
		var half = location.search.split('&' + n + '=')[1];
		if (!half) half = location.search.split('?' + n + '=')[1];
		return half !== undefined ? decodeURIComponent(half.split('&')[0]) : null;
	}
	jQuery( document ).ready(function($) {
		var updated = get('updated');
		if (updated=='true'){
			$('html, body').animate({
				scrollTop: $('#xform').offset().top
			}, 1000);
		}
	});
	</script>
	";
?>   