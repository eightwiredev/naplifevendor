<?php if ( have_rows('layout') ): ?>
<section class="layouts cf">
<?php
    while ( have_rows('layout') ) : the_row();
    
		get_template_part('inc/layout', get_row_layout() );
	
    endwhile;
?>   
</section>
<?php endif; ?>