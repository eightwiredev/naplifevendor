<?php
    $header = get_field('header');
    $text = get_field('text');

if ( is_woocommerce() ):
    $term_object = get_queried_object();    
    $header = woocommerce_page_title( false );
    //$header = $term_object->name;
    $text = $term_object->description;
    if ($term_object->name == 'product') {
         $text = get_field('strapline', 230);
    }
if (!$header){
    global $product;
    $seller = get_post_field( 'post_author', $product->get_id());
    $author  = get_user_by( 'id', $seller );
    $store_info = dokan_get_store_info( $author->ID );
    if ( !empty( $store_info['store_name'] ) ) {
        $header = $store_info['store_name'];
    }  
}
endif;

if ( is_search() ) {
	$text='';
}


if ( is_search() ){ $header = "Search results"; }

if (!$header){ $header = get_the_title(); }



    echo '<div class="page-banner">';
        if ( is_woocommerce() ):
            echo '<div class="page-banner-breadcrumbs">';
                woocommerce_breadcrumb();
            echo '</div>';
        endif;

        echo '<div class="page-banner-header flex">';
            echo '<h3>'.$header.'<img class="banner-leaf" src="'.get_template_directory_uri().'/assets/images/leaf.svg" /></h3>';
        echo '</div>';
        echo '<p>'.$text.'</p>';
    echo '</div>';
?>