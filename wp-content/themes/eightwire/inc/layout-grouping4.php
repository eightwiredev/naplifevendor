<?php 
$secondarytext=get_sub_field('secondary_text');
$photos=get_sub_field('photos');
$maintext=get_sub_field('main_text');
$link=get_sub_field('link');
$alttext=get_sub_field('alt_text');
$link2=get_sub_field('link_2');
?>
<section class="igrid">
    <div class="q i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[0]['sizes']['photoyy'].');"';
        endif; ?>></div>
    <div class="q s c"><div><?php echo $secondarytext; ?></div></div>
    <div class="q i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[1]['sizes']['photoyy'].');"';
        endif; ?>></div>
    <div class="q i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[2]['sizes']['photoyy'].');"';
        endif; ?>></div>
    <div class="h i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[3]['sizes']['photoyy'].');"';
        endif; ?>></div>
    <div class="h z"><div><?php 
        echo $maintext;
        if ($link):
            echo '<a href="'.$link["url"].'" target="'.$link["target"].'" class="btn-booknow">'.$link["title"].'</a>';
        endif; ?></div></div>
    <div class="h i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[4]['sizes']['photoyy'].');"';
        endif; ?>></div>
    <div class="h z alt"><div><?php 
        echo $alttext;
        if ($link2):
            echo '<a href="'.$link2["url"].'" target="'.$link2["target"].'" class="btn-booknow">'.$link2["title"].'</a>';
        endif; ?></div></div>
</section>