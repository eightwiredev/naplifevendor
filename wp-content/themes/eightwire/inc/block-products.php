<?php
    $header = get_field('header', 8);
    echo '<div class="products-header">';
        echo '<h3>'.$header.'<img class="product-leaf" src="'.get_template_directory_uri().'/assets/images/leaf.svg" /></h3>';
    echo '</div>';

    if (have_rows('images', 8)) {
        echo '<div class="product-images flex flexwrap">';
        while (have_rows('images', 8)) {
            the_row();
            $image = get_sub_field('image');
            $pos = get_sub_field('position');
            
            echo '<div style="background-image:url('.$image["sizes"]["media"].');background-position:'.$pos.'"></div>';
        }
        echo '</div>';
    }
?>