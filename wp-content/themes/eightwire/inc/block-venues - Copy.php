<section class="venues-section">
        <div class="wrapper">
	<div class="venues-panels">
        <div class="venues-panel">
        	<div class="venues-panel-inner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/panel1.jpg);">
                <div class="venues-panel-pre">
                    <h4>Weston Hall</h4>
                    <ul>
                        <li>Luxury, vintage style weddings</li>
                        <li>Capacity of up to 300 guests</li>
                        <li>Perfect for Foodies</li>
                    </ul>
                    <a href="#contact-us">SEE MORE</a>
                </div>
            </div>
        </div>
        <div class="venues-panel">
        	<div class="venues-panel-inner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/panel2.jpg);">
                <div class="venues-panel-pre">
                <h4>Sywell Grange</h4>
                <ul>
                    <li>Rolling country backdrop</li>
                    <li>Capacity of up to 300 guests</li>
                    <li>Very few restrictions</li>
                </ul>
                <a href="#contact-us">SEE MORE</a>
                </div>
            </div>
        </div>
        <div class="venues-panel">
        	<div class="venues-panel-inner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/panel3.jpg);">
                <div class="venues-panel-pre">
                <h4>Lyveden New Bield</h4>
                <ul>
                    <li>Atmospheric historic setting</li>
                    <li>Capacity of up to 200 guests</li>
                    <li>Magical photography opportunities</li>
                </ul>
                <a href="#contact-us">SEE MORE</a>
                </div>
            </div>
        </div>
        <div class="venues-panel">
        	<div class="venues-panel-inner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/panel4.jpg);">
                <div class="venues-panel-pre">
                <h4>The Grange Field</h4>
                <ul>
                    <li>DIY wedding dream setting</li>
                    <li>Completely flexible</li>
                    <li>Stunning country backdrop</li>
                </ul>
                <a href="#contact-us">SEE MORE</a>
                </div>
            </div>
        </div>
        <div class="venues-panel">
        	<div class="venues-panel-inner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/panel5.jpg);">
                <div class="venues-panel-pre">
                <h4>Harlestone Park </h4>
                <ul>
                    <li>Capacity of up to 110</li>
                    <li>Perfect for golf enthusiasts</li>
                    <li>Pretty church within the grounds</li>
                </ul>
                <a href="#contact-us">SEE MORE</a>
                </div>
            </div>
        </div>
        <div class="venues-panel">
        	<div class="venues-panel-inner">
                <div class="venues-panel-post">
            	<h4>Have you got a beautiful wedding venue?</h4>
                <p>If you have a beautiful building or stunning grounds in the Midlands or South West which are the perfect setting for a wedding, contact Country & Coast Wedding to see if we can help. We look after all the planning and management required when hosting weddings and occasions.</p>
				<a href="#contact-us">CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
    <a href="<?php the_permalink(99);?>" class="venues-cta"><h5><span>CLICK HERE TO&nbsp;</span>SEE MORE VENUES</h5><hr /></a>
    </div>
</section>




<?php
	if (function_exists('have_rows')) {
		if (have_rows('panels')) { ?>
			<section class="panels cf">
						<div class="wrapper cf">
					<?php while( have_rows('panels') ) {
						the_row();
						$link = get_sub_field('link');
						$caption = get_sub_field('caption');
						$image = get_sub_field('image');
						$excerpt = get_sub_field('excerpt');
						$banner_img = $image['sizes']['panel'];
						?>
						<div class="panel cf">
							<?php if ($link) { ?><a href="<?php echo $link['url']; ?>"> <?php } ?>
                                <?php echo '<img src="'.$banner_img.'" />'; ?>
                                    <?php if ($caption) { ?>
                                        <h3 class="panel_caption">
                                            <?php echo $caption; ?> >
                                        </h3>
                                    <?php } ?>
                                    <?php if ($excerpt) { ?>
                                        <div class="panel_excerpt">
									<?php if ($caption) { ?>
                                        <h3 class="panel_excerpt_caption">
                                            <?php echo $caption; ?>
                                        </h3>
                                    <?php } ?>
                                    <?php echo $excerpt; ?>
                                    <h3>></h3>
                                        </div>
                                    <?php } ?>
							<?php if ($link) { ?></a><?php } ?>
						</div>
					<?php } ?>
</div>
</section>	
<?php
		}
	}
?>
