<?php 
$vid=get_sub_field('video');//pS_HACD4ENg
$pic=get_sub_field('photo');
if ($vid || $pic) { $textside = get_sub_field('textside'); }else{ $textside = 'center'; }
echo '<div class="wrapper cf">';
echo '<div class="layout media-'.$textside.' cf">';
	echo '<div class="layout-two">';
			if ($vid || $pic) {
				echo '<div class="layout-one">';
				echo '<div class="transmedia">';
				if ($vid) {
					//vid
					echo '<div class="ratio">';
					echo '<iframe src="//www.youtube.com/embed/'.$vid.'?rel=0&amp;autoplay=1&amp;modestbranding=1&amp;iframe=true&amp;allowfullscreen=true" scrolling="no" allowfullscreen="yes"></iframe>';
					echo '</div>';
				}else{
					if ($pic) {
						//pic
						//echo '<a href="'.$pic["sizes"]["imagex"].'" class="ewmodal-btn-open"><img src="'.$pic["sizes"]["media"].'" /></a>';
						echo '<img src="'.$pic["sizes"]["media"].'" />';
					}
				}
				echo '</div>';
				echo '</div>';
			}
		$title = get_sub_field('title');
		if ($title):
			echo '<h2>'.$title.'</h2>';
		endif;
		echo '<div class="innertext">';
		$text = get_sub_field('text');
		if ($text):
			echo $text;
		endif;
		echo '</div>';
	echo '</div>';
echo '</div>';
echo '</div>';
?>