<?php if( have_rows('testimonialsx') ): ?>

	<section class="testimonials flexslider cf">
    
<div class="wrapper cf">
    
    <h2>Testimonials</h2>
    
		<div class="slides">
    
	<?php while( have_rows('testimonialsx') ): the_row(); 

		$quote = get_sub_field('quote');
		$by = get_sub_field('by');
		$date = get_sub_field('date');

		?>

		<div class="testimonial">
		<div class="testimonial-inner">

		    <p class="quote"><?php echo $quote; ?></p>
            <?php 
			if ($by) { 
				echo '<p class="byline"><span class="by">'.$by.'</span>'; 
				if ($date) { 
					echo ', <span class="date">'.$date.'</span>'; 
				} 
				echo '</p>'; 
			} 
			?>

		</div>
		</div>

	<?php endwhile; ?>
    
		</div>
        
		</div>
  
	</section>

<?php endif; ?>