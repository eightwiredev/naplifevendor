<?php if( have_rows('eventsx') ): ?>

	<section class="testimonials events flexslider cf">
    
<div class="wrapper cf">
    
    <h2>Events</h2>
    
		<div class="slides">
    
	<?php while( have_rows('eventsx') ): the_row(); 

		$quote = get_sub_field('event');
		$by = get_sub_field('link');
		$date = get_sub_field('date');

		?>

		<div class="testimonial">
		<div class="testimonial-inner">

		    <p class="quote"><?php echo $quote; ?></p>
            <?php 
				echo '<p class="byline">'; 
				if ($date) { 
					echo '<span class="date">'.$date.'</span> '; 
				} 
				if ($by) { 
					echo ' <a href="'.$by.'" target="_blank" class="by">View more</a>'; 
				} 
				echo '</p>'; 
			?>

		</div>
		</div>

	<?php endwhile; ?>
    
		</div>
        
		</div>
  
	</section>

<?php endif; ?>