<?php 
$title=get_sub_field('title');
$maintext=get_sub_field('main_text');
$photos=get_sub_field('photos');
//echo count($photos);
$secondarytext=get_sub_field('secondary_text');
$link=get_sub_field('link');
?>
<section class="igrid flipped">
    <div class="q i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[0]['sizes']['photoyy'].');"';
        endif; ?>></div>
    <div class="q i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[1]['sizes']['photoyy'].');"';
        endif; ?>></div>
    <div class="q y"><div><?php echo $maintext; ?></div></div>
    <div class="q i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[2]['sizes']['photoyy'].');"';
        endif; ?>></div>
    <div class="h z alt"><div><?php 
        echo $secondarytext;
        if ($link):
            echo '<a href="'.$link["url"].'" target="'.$link["target"].'" class="btn-booknow">'.$link["title"].'</a>';
        endif; ?></div></div>
    <div class="h i"<?php if ($photos):
            echo ' style="background-image:url('.$photos[3]['sizes']['photoyy'].');"';
        endif; ?>></div>
</section>