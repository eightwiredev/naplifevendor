<?php 
$image=get_sub_field('image');
$fullwidth=get_sub_field('fullwidth');
if (!$fullwidth) { 
	echo '<div class="wrapper cf">';
}
echo '<section class="layout image cf">';
					if ($image) {
						echo '<img src="'.$image["sizes"]["image"].'" />';
					}
	echo '</section>';
if (!$fullwidth) { 
	echo '</div>';
}
?>