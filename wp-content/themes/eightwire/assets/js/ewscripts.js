jQuery(document).ready(function ($) {
	$('.flexslider.banner').flexslider({
		selector: ".slides > li",
		animation: "fade",
		controlNav: false,
		easing: "linear",
		useCSS: true,
		directionNav: true,
		slideshow: true,
		slideshowSpeed: "7000",
		animationSpeed: "600",
		randomize: false,
		pauseOnHover: true,
		prevText: "",
		nextText: "",
		after: function(){
			onYouTubeIframeAPIReady();
		}
	});
	
	
/* Youtube API */
/*	function onYouTubeIframeAPIReady() {
	    var player;
	    player = new YT.Player('youtubevideo', {
		videoId: 'AVuL29w_P3s', // YouTube Video ID
		width: 1920,               // Player width (in px)
		height: 1080,              // Player height (in px)		
		playerVars: {
		  rel: 0,        	      // No related
		  autohide: 1,             // Hide video controls when playing
		  controls: 0,            // Show pause/play buttons in player
		  showinfo: 0,            // Hide the video title
		  hd: 1,                  // HD
		  modestbranding: 1,      // Hide the Youtube Logo
		  iv_load_policy: 3,      // Hide the Video Annotations
		  cc_load_policy: 0,      // Hide closed captions
		  color: 'white',         // White or Red playbar
		  iframe: true,           // iFrame
		  allowfullscreen: true,  // Fullscreen
		  fs: 1,                  // The full screen button
		  loop: 1,                // Run the video in a loop
		  playlist: 'AVuL29w_P3s',// Playlist
		  list: 'AVuL29w_P3s',    // Playlist
		  autoplay: 1,            // Auto-play the video on load
		},
		events: {
		  onReady: function(e) {
			e.target.mute();
		  }
		}
	  });
	 }
*/	
	$('.testimonials .flexslider').flexslider({
		selector: ".slides > .slide",
		animation: "fade",
		animationLoop: true,
		controlNav: false,
		easing: "linear",
		useCSS: true,
		directionNav: true,
		slideshow: true,
		slideshowSpeed: "4000",
		animationSpeed: "600",
		randomize: true,
		pauseOnHover: true,
		prevText: "",
		nextText: ""
	});

	$('.flexslider.thumbslider').flexslider({
		selector: ".slides > li",
		animation: "slide",
		controlNav: false,
		easing: "linear",
		useCSS: true,
		directionNav: true,
		slideshow: true,
		slideshowSpeed: "7000",
		animationSpeed: "600",
		randomize: false,
		pauseOnHover: true,
		prevText: "",
		nextText: "",
		minItems: 2,
		maxItems: 4,
		itemWidth: 280,
		itemMargin: 10,
		move: 1
	});
	
	// http://dimsemenov.com/plugins/magnific-popup/documentation.html
	$('.lightbox').magnificPopup({
		delegate: 'a',
	  	type: 'image',
	  	gallery:{
			enabled:true
	  	}
	});
	
	//$(function() {
		$('.equal').matchHeight({
			byRow: true,
			property: 'height',
			target: null,
			remove: false
		});
	//});
    
    if ( $("ol").hasClass("flex-control-nav") ) {
      $(".woo-output .woocommerce-product-gallery").addClass('hascontrolnav');
    }


	//Scroll on internal links
  $('a[href^="#"]').on('click', function (event) {
    event.preventDefault();

    var target = this.hash;

		//Don't scroll on tab links
		if (!target.substr(0, 4) == '#tab') {
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
		}
	});
});


                                          var tag = document.createElement('script');
                                    
                                          tag.src = "https://www.youtube.com/iframe_api";
                                          var firstScriptTag = document.getElementsByTagName('script')[0];
                                          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                                    
                                          //var player;
                                          var players;
										  
										    function onYouTubeIframeAPIReady() {
												players = jQuery('.video-fg iframe');
												for (var i = 0; i < players.length; i++) {
													new YT.Player(players[i], {
														playerVars: {
															'autoplay': 1,
															'loop': 1,
															'mute': 1,
															'playlist': players[i].dataset.id,
															'list': players[i].dataset.id
														},
														videoId: players[i].dataset.id,
														events: {
														  onReady: function(e) {
															e.target.playVideo();
															e.target.mute();
														  }
														}
													});
												}
											}