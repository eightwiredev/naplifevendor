<?php get_header(); ?>
<main class="main" role="main">
    <article class="article">
        <div class="wrapper cf">
            <?php get_template_part('inc/block', 'page-banner');?>
            <?php get_template_part('inc/block', 'layout');?>
            <?php get_template_part('inc/block', 'get-inspired');?>
        </div>
    </article>
</main>
<?php get_footer(); ?>