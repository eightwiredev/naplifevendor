<?php get_header(); ?>
<main class="main" role="main">
    <article class="article">
        <div class="wrapper cf">
            <?php get_template_part('inc/block', 'page-banner');?>
            <section class="vendor-banner">
                <div class="vendor-button-container">
                    <div class="btn yellow-btn"><a href=""><p>DOWNLOAD OUR VENDOR PACK</p></a></div>
                </div>                
            </section>
            <?php get_template_part('inc/block', 'layout');?>
            <?php get_template_part('inc/block', 'partner-banner');?>
            <?php get_template_part('inc/block', 'products');?>
        </div>
    </article>
</main>
<?php get_footer(); ?>