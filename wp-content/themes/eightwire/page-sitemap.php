<?php get_header(); ?>
<main class="main" role="main">
  <?php if (have_posts()) { ?>
    <article class="article">
      <div class="wrapper cf">
        <?php while (have_posts()) {
          the_post(); ?>

  <section class="media-text">
   <h3>SITEMAP</h3>
         <div class="cols col-wrap">
         <div class="col-one">
            <?php the_content(); ?><br>
            <h4>PAGES</h4>
            <?php
              $pargs = array(
                'post_type'       => 'page',
                'post_parent'     => 0,
                'posts_per_page'  => -1,
                'orderby'         => 'menu_order',
                'order'           => 'ASC'
              );

              $page_q = new WP_Query($pargs);

              if ($page_q->have_posts()) { ?>
                <ul class="sitemap">
                <?php while ($page_q->have_posts()) {
                  $page_q->the_post(); ?>
                  <li class="page-item">
                    <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                    <?php
                      $children = get_pages('child_of=' . $post->ID);
                      if (count($children) != 0) { ?>
                        <ul class="sub-page">
                          <?php
                            foreach ($children as $child) {
                              echo '<li class="page-item child-page"><a href="' . get_the_permalink($child->ID) . '">' . $child->post_title . '</a></li>';
                            }
                          ?>
                        </ul>
                      <?php }
                    ?>
                  </li>
                <?php } ?>
                </ul>
              <?php }
            wp_reset_postdata(); ?>
      </div>
         <div class="col-two">
            <?php the_content(); ?><br>
            <h4>EVENTS</h4>
            <?php
              $pargs = array(
                'post_type'       => 'post',
                'post_parent'     => 0,
                'posts_per_page'  => -1,
                'orderby'         => 'date',
                'order'           => 'DESC'
              );

              $page_q = new WP_Query($pargs);

              if ($page_q->have_posts()) { ?>
                <ul class="sitemap">
                <?php while ($page_q->have_posts()) {
                  $page_q->the_post(); ?>
                  <li class="page-item">
                    <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                    <?php
                      $children = get_pages('child_of=' . $post->ID);
                      if (count($children) != 0) { ?>
                        <ul class="sub-page">
                          <?php
                            foreach ($children as $child) {
                              echo '<li class="page-item child-page"><a href="' . get_the_permalink($child->ID) . '">' . $child->post_title . '</a></li>';
                            }
                          ?>
                        </ul>
                      <?php }
                    ?>
                  </li>
                <?php } ?>
                </ul>
              <?php }
            wp_reset_postdata(); ?>
      </div>
      </div>
  </section>
        <?php } ?>
        </div>
    </article>
  <?php } else { ?>
    <article class="article">
    </article>
  <?php } ?>
</main>
<?php get_footer(); ?>
