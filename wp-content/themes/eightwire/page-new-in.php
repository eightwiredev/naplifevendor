<?php get_header(); ?>
<main class="main" role="main">
  <article class="article">
        <div class="wrapper cf">
            <?php get_template_part('inc/block', 'page-banner');?>
            <div class="page-content">
                <div class="woo-wrap">
                    <?php //woocommerce_breadcrumb(); ?>
                     <?php get_sidebar(); ?>
                    <section class="woo-output">
                <?php echo do_shortcode('[products limit="20" columns="4" orderby="id" order="DESC" visibility="visible"]'); ?>

                    </section>
                </div>
            </div>                
            <section class="poprec">
                <?php
                    echo '<div class="page-banner"><h3>Popular Right now<img class="banner-leaf" src="/wp-content/themes/eightwire/assets/images/leaf.svg"></h3></div>';
                    echo '<div class="page-content">' . do_shortcode('[products limit="4" columns="4" best_selling="true" ]') . '</div>';
                    echo ew_recently_viewed_products(5);
                ?>
            </section>

        </div>
    </article>
</main>
<?php get_footer(); ?>
