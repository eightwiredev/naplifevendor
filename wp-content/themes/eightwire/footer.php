<footer class="footer cf">
    <div class="wrapper flex flexstretch flexwrap">
        <section class="footericon">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/footer-logo.svg" />
        </section>   
        <section class="footernav">
            <?php wp_nav_menu(array('theme_location' => 'secondary-menu', 'container' => false)); ?>
        </section>
        <section class="footercontact">
          <span>Contact Us</span>
            <a href="tel:+447973593340">07973 593340</a>
            <a href="mailto:info@chooseconscious.com">info@chooseconscious.com</a>
	  </section>
		<section class="footerconnect">
            <h6>CONNECT WITH US</h6>
            <div class="social-icons flex flexwrap">
                <?php
                    $facebook = get_field('facebook', 'option');
                    $twitter = get_field('twitter', 'option');
                    $insta =get_field('instagram', 'option');
                    $pin = get_field('pinterest', 'option');
                
                    if ($facebook) {
                        echo '<a class="social" href="'.$facebook.'" target="_blank">';
                            echo facebook_svg();
                        echo '</a>';
                    }
                
                    if ($twitter) {
                        echo '<a class="social" href="'.$twitter.'" target="_blank">';
                            echo twitter_svg();
                        echo '</a>';
                    }
                
                    if ($insta) {
                        echo '<a class="social" href="'.$insta.'" target="_blank">';
                            echo instagram_svg();
                        echo '</a>';
                    }
                    
                    if ($pin) {
                        echo '<a class="social" href="'.$pin.'" target="_blank">';
                            echo pin_svg();
                        echo '</a>';
                    }
                ?>
            </div>
		</section>
        <section class="footer-logos flex flexwrap">
            <div class="footer-logo-img">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/b-corp-logo.png" class="b-corp" />
            </div>
            <div class="footer-logo-img">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/gg-logo.png" class="gg" />
            </div>
        </section>
        <section class="footer-buttons flex flexwrap">
            <div class="footer-button" id="partner-btn">Partner with us</div>
            <?php 
            if (is_user_logged_in()) { ?>
            <a href="/my-account" class="footer-button">My account</a>
                
             <?php
             } else {
            ?>
            <a href="/my-account" class="footer-button">Register</a>
            <?php 
            }
            ?>
            <div class="footer-button" id="newsletter-btn">Newsletter</div>
            <?php 
            if (!is_user_logged_in()) {
            ?>
            <a href="/my-account" class="footer-button">Sign in</a>
            <?php
            }
            ?>
        </section>
    </div>
    
    <div class="footercredits">
        <p>All Content © Choose Conscious <?php echo date('Y');?> | <a href="https://eightwire.uk" target="_blank">Website designed by © Eight Wire</a></p>
    </div>
</footer>

<div id="modalbg" class="modal-bg">
</div>

<div id="modalWindow" class="modal-window">
  <div id="modalClose" class="modal-close">
    <span>&times;</span>
    <h5>Close</h5>
  </div>

  <div id="modalContent" class="modal-content"></div>

</div>

<?php wp_footer(); ?>
</body>
</html>