<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141109005-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-141109005-1');
</script>
<meta name="p:domain_verify" content="e773c3bdd6bc9703bbfc2ae935246c2d"/>
    
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="HandheldFriendly" content="True" />
  <meta name="MobileOptimized" content="320" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="theme-color" content="#d1a239">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <header class="header">
    <div class="wrapper cf">
        <div class="toprow flex flexwrap">
            <a href="/" class="sitelogo" itemprop="url">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/site-logo.svg" alt="<?php echo get_bloginfo('name'); ?>" itemprop="logo" />
            </a>
            <div class="header-nav">
                <div class="header-shop flex flexwrap">
                    <?php echo '<a class="basket-btn" href="'.wc_get_cart_url().'"><img src="'.get_template_directory_uri().'/assets/images/basket-i.svg" />SHOPPING BASKET (' . WC()->cart->get_cart_contents_count() . ')</a>'; ?>
                </div>
                <nav class="nav cf flex" role="navigation">
                    <?php wp_nav_menu( array('theme_location' => 'shop-menu', 'container' => false )); ?>
                </nav>
            </div>
        </div>
        <div class="header-yellow"></div>
        <div class="menu-bar flex flexwrap">
            <?php get_search_form();  //get_product_search_form(); ?>
            <?php wp_nav_menu( array('theme_location' => 'primary-menu', 'container' => false )); ?>
        </div>

        <div id="modalMenuOpen" class="mobile-nav-open">
            <!-- <i class="icon icon-menu ewmodal-btn-open" data-target="mobile-nav" role="img" aria-hidden="true"></i>-->
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/menu.svg" alt="MENU" width="40" height="40" class="ewmodal-btn-open" data-target="mobile-nav" />
        </div>

    </div>

  </header>

  <div id="modalMenu" class="mobile-nav">
    <div class="menu-popout-wrap"><br>
        <a href="/" itemprop="url">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/site-logo.svg" alt="<?php echo get_bloginfo('name'); ?>" itemprop="logo" />
                </a>
        <br>
      <?php wp_nav_menu(array('theme_location' => 'shop-menu', 'menu_class' => 'nav-menu')); ?><br><br>
        <?php wp_nav_menu( array('theme_location' => 'primary-menu', 'container' => false )); ?>
        <br>
      <div id="modalMenuClose" class="ewmodal-btn-close">
        <h5><i class="icon icon-close" role="img" aria-hidden="true">&times;</i></h5>
      </div>
    </div>
  </div>
    
    <div id="news-modal" class="modalpanel">
        <div id="news-close">&times;</div>
        <h3>Sign up to our newsletter</h3>
        <?php echo do_shortcode('[contact-form-7 id="109" title="Newsletter Sign Up"]');?>
    </div>
    
    <div id="partner-modal" class="modalpanel">
        <div id="partner-close">&times;</div>
        <h3>Partner with us</h3>
        <div class="contact-form">
            <?php echo do_shortcode('[contact-form-7 id="100" title="Contact Form"]'); ?>
        </div>
    </div>
    
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#newsletter-btn').click(function (event) {
                
               ewShowModal();
                $('#news-modal').addClass('active');
            });
            
            $('#news-close').click(function(event) {
               ewHideModal(); 
            });
            
            $('#partner-btn').click(function (event) {
                
               ewShowModal();
                $('#partner-modal').addClass('active');
            });
            
            $('.partner-button').click(function (event) {
                
               ewShowModal();
                $('#partner-modal').addClass('active');
            });
            
            $('#partner-close').click(function(event) {
               ewHideModal(); 
            });
            
            function ewHideModal() {
                $('#modalbg').removeClass('active');
                $('html').removeClass('locked');
                  $('#news-modal').removeClass('active');  
                  $('#partner-modal').removeClass('active');
              }
            
            function ewShowModal() {
                $("#modalbg").addClass('active');
                $('html').addClass('locked');
              }
        });
    </script>
