<?php get_header(); ?>
<main class="main" role="main">
  <?php if (have_posts()) { ?>
    <article class="article">
<div class="wrapper cf">
  <h3><?php echo strtoupper(get_the_title()); ?></h3>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/grey-logo.png" alt="" class="heading-logo" />
        <?php while (have_posts()) {
          the_post(); ?>

          <section class="content">
          <?php
              the_post_thumbnail('photoyy');
            ?>
			<?php the_content(); ?></section>
        <div class="custom-pagination cf">
            <div class="alignleft">
                <?php next_post_link('%link','< Previous'); ?>
            </div>
        <div class="alignmid">
            <a href="../">Back ^</a>
         </div>
         <div class="alignright">
                <?php previous_post_link('%link','Next >'); ?>
            </div>
        </div>
        </div>
        <?php } ?>
    </article>
  <?php } else { ?>
    <article class="article">
    </article>
  <?php } ?>
</main>
<?php get_footer(); ?>
