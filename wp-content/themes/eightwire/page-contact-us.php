<?php get_header(); ?>
<main class="main" role="main">
    <article class="article">
        <div class="wrapper cf">
            <section class="contact-banner">
                <div class="contact-banner-container flex">
                    <h3><img class="left-leaf" src="<?php echo get_template_directory_uri();?>/assets/images/left-leaf.svg" />CONTACT US</h3>
                </div>
            </section>
            <section class="contact-wrapper">
                <div class="contact-form">
                    <?php echo do_shortcode('[contact-form-7 id="100" title="Contact Form"]');?>
                </div>
            </section>
            <section class="connect-banner">
                <h4>Connect with us</h4>
                <div class="social-icons flex flexwrap">
                <?php
                    $facebook = get_field('facebook', 'option');
                    $twitter = get_field('twitter', 'option');
                    $insta =get_field('instagram', 'option');
                    $pin = get_field('pinterest', 'option');
                
                    if ($facebook) {
                        echo '<a class="social" href="'.$facebook.'">';
                            echo facebook_svg();
                        echo '</a>';
                    }
                
                    if ($twitter) {
                        echo '<a class="social" href="'.$twitter.'">';
                            echo twitter_svg();
                        echo '</a>';
                    }
                
                    if ($insta) {
                        echo '<a class="social" href="'.$insta.'">';
                            echo instagram_svg();
                        echo '</a>';
                    }
                    
                    if ($pin) {
                        echo '<a class="social" href="'.$pin.'">';
                            echo pin_svg();
                        echo '</a>';
                    }
                ?>
            </div>
            </section>
            <?php get_template_part('inc/block', 'partner-banner');?>
            <?php get_template_part('inc/block', 'newsletter-banner');?>
        </div>
    </article>
</main>
<?php get_footer(); ?>