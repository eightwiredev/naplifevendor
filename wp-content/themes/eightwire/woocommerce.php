<?php get_header(); ?>
<main class="main" role="main">
  <article class="article">
        <div class="wrapper cf">
            <?php get_template_part('inc/block', 'page-banner');?>
            <div class="page-content">
                <div class="woo-wrap">
                    <?php //woocommerce_breadcrumb(); ?>
                     <?php get_sidebar(); ?>
                    <section class="woo-output">
                        <?php echo do_shortcode('[wpf-filters id=2]'); 
                        if ( is_shop() ) :
                            echo do_shortcode('[product_categories number="0" parent="0" columns="5" hide_empty="1"]');
                        else:
                            woocommerce_content();
                        endif; 
                        ?>
                    </section>
                </div>
            </div>                
            <section class="poprec">
                <?php
                if ( is_shop() ) :
                    echo '<div class="page-banner"><h3>Popular Right now<img class="banner-leaf" src="/wp-content/themes/eightwire/assets/images/leaf.svg"></h3></div>';
                    echo '<div class="page-content">' . do_shortcode('[products limit="4" columns="4" best_selling="true" ]') . '</div>';
                    echo ew_recently_viewed_products(4);
                endif;
                if ( is_product_category() ) :
                    echo '<div class="page-banner"><h3>Popular Right now<img class="banner-leaf" src="/wp-content/themes/eightwire/assets/images/leaf.svg"></h3></div>';
                    echo '<div class="page-content">' . do_shortcode('[products limit="4" columns="4" best_selling="true" ]') . '</div>';
                    echo ew_recently_viewed_products(4);
                endif;
                if ( is_product() ) :
                    echo '<div class="page-banner"><h3>Related Products<img class="banner-leaf" src="/wp-content/themes/eightwire/assets/images/leaf.svg"></h3></div>';
                    echo '<div class="page-content">' . do_shortcode('[related_products limit="4" columns="4" best_selling="true" ]') . '</div>';
                    echo ew_recently_viewed_products(4);
                endif;
                ?>
            </section>

        </div>
    </article>
</main>
<?php get_footer(); ?>
