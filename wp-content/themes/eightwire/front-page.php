<?php get_header(); ?>
<main class="main" role="main">
    <article class="article">
        <div class="wrapper cf">
            <?php get_template_part('inc/block', 'home-banner');?>
            <?php get_template_part('inc/block', 'layout');?>
            <?php get_template_part('inc/block', 'home-info-trio');?>
            <?php get_template_part('inc/block', 'product-banner');?>
        </div>
    </article>
</main>
<?php get_footer(); ?>