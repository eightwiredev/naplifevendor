<?php get_header(); ?>
<main class="main" role="main">
    <article class="article">
        <div class="wrapper cf">
            <?php get_template_part('inc/block', 'page-banner');?>
            <section class="contact-banner">
                <div class="contact-banner-container">
                    <h3><img class="left-leaf" src="<?php echo get_template_directory_uri();?>/assets/images/left-leaf.svg" />CONTACT US</h3>
                </div>
            </section>
            <section class="contact-wrapper">
                <div class="contact-form">
                    <?php echo do_shortcode('[contact-form-7 id="100" title="Contact Form"]');?>
                </div>
            </section>
            <?php get_template_part('inc/block', 'products');?>
        </div>
    </article>
</main>
<?php get_footer(); ?>