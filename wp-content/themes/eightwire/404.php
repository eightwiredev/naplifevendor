<?php get_header(); ?>
<main class="main" role="main">
  <div class="wrapper">
    <article class="article">
        <div class="page-banner">
            <h3>404 - PAGE NOT FOUND<img class="banner-leaf" src="<?php echo get_template_directory_uri();?>/assets/images/leaf.svg" /></h3>
        </div>
    </article>
  </div>
</main>
<?php get_footer(); ?>
