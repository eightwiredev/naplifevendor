<?php 
	if (function_exists('have_rows')) {
		if (have_rows('panels')) { $i=0; ?>
			<section class="panels">
						<div class="wrapper cf">
					<?php while( have_rows('panels') ) {
						the_row();
						$link = get_sub_field('link');
						$excerpt = get_sub_field('excerpt');
						if($i==0){$panelclass=' p1';}
						if($i==1){$panelclass=' p2';}
						if($i==2){$panelclass=' p3';}
						?>
						<div class="panel<?php echo $panelclass; ?>">
							<?php if ($link) { ?><a href="<?php echo $link['url']; ?>"> <?php } 
							echo $link['title'];
							 if ($link) { ?></a><?php } ?>

                                    <?php if ($excerpt) { ?>
                                            <p><?php echo $excerpt; ?></p>
                                    <?php } ?>
                                    <?php echo $excerpt; ?>
	                        </div>
					<?php $i++; } ?>
        </div>
        </section>	
<?php
		}
	}
?>
